# Shell Projects

This repository contains custom shell projects designed to accomplish myriad tasks. The projects have been tested on the following GNU/Linux distributions using the [Bash](https://www.gnu.org/software/bash/) shell:

- [Debian](https://www.debian.org/)
- [Fedora](https://getfedora.org/)

## Dot Files

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="dot_files/">Dot Files</a></td>
      <td>Shell configuration dot files.</td>
    </tr>
  </tbody>
</table>

## Scripts

For scripts that interact with a desktop environment, [GNOME](https://www.gnome.org/) is assumed.

Instructions on how to execute scripts can be found at the [Linux Documentation Project](https://tldp.org/LDP/Bash-Beginners-Guide/html/sect_02_01.html).

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Required Packages/Scripts</th>
      <th>Variables to Set</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="scripts/act_ve/">Activate Virtual Environment</a></td>
      <td>Activate a Python 3 virtual environment.</td>
      <td></td>
      <td><em>projs_dir</em>, <em>projs_dir_fltr</em>, <em>ves_dir</em>, <em>ves_dir_fltr</em></td>
    </tr>
    <tr>
      <td><a href="scripts/batch_rename/">Batch Object Renaming</a></td>
      <td>Change file extensions, or prepend/append text to file system object names.</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="scripts/crt_demo_data/">Create Demo Data</a></td>
      <td>Create demo data directory.</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="scripts/crt_timer/">Create Timer</a></td>
      <td>Create new systemd timer.</td>
      <td></td>
      <td><em>script_editor</em></td>
    </tr>
    <tr>
      <td><a href="scripts/crt_ve/">Create Virtual Environment</a></td>
      <td>Create a new Python 3 virtual environment.</td>
      <td>Debian: <em>python3-pip</em>, <em>python3-venv</em><br />Fedora: <em>python3-pip</em></td>
      <td><em>projs_dir</em>, <em>projs_dir_fltr</em>, <em>ves_dir</em></td>
    </tr>
    <tr>
      <td><a href="scripts/estab_conn/">Established Connections</a></td>
      <td>Display established connection information.</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="scripts/find_f_paths/">Find File Paths</a></td>
      <td>Find file paths by filename text or file content.</td>
      <td><em>pdfgrep</em></td>
      <td><em>o_srch_type</em>, <em>srch_dir</em></td>
    </tr>
    <tr>
      <td><a href="scripts/fwd_mgmt/">firewalld Management</a></td>
      <td>Manage common firewalld tasks.</td>
      <td>Debian: <em>firewalld</em></td>
      <td></td>
    </tr>
     <tr>
      <td><a href="scripts/flpk_mgmt/">Flatpak Management</a></td>
      <td>Manage flatpak assets.</td>
      <td>Debian: <em>flatpak</em></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="scripts/frz_pkgs/">Freeze Virtual Environment Packages</a></td>
      <td>Freeze packages for Python virtual environments.</td>
      <td><em>python3-pip</em></td>
      <td><em>o_req_dir</em>, <em>ves_dir</em>, <em>ves_dir_fltr</em></td>
    </tr>
    <tr>
      <td><a href="scripts/git_projs/">Git Projects</a></td>
      <td>Manage various Git-based projects.</td>
      <td>Debian: <em>git</em></td>
      <td><em>dir_options</em>, <em>script_editor</em></td>
    </tr>
    <tr>
      <td><a href="scripts/hw_info/">Hardware Information</a></td>
      <td>Display hardware diagnostic information.</td>
      <td></td>
      <td><em>scripts_dir</em></td>
    </tr>
    <tr>
      <td><a href="scripts/loc_host_tests/">Local Host Tests</a></td>
      <td>Provide a local host network test battery.</td>
      <td>Debian: <em>speedtest-cli</em>, <em>tcpdump</em><br />Fedora: <em>speedtest-cli</em><br />Common: <em>estab_conn.bash</em></td>
      <td><em>dump_dir</em>, <em>scripts_dir</em></td>
    </tr>
    <tr>
      <td><a href="scripts/pkg_mgmt/">Package Management</a></td>
      <td>Assist with package management tasks.</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
    <tr>
      <td><a href="scripts/proc_mgmt/">Process Management</a></td>
      <td>Display and manage processes.</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="scripts/rem_host_tests/">Remote Host Tests</a></td>
      <td>Provide a remote host network test battery.</td>
      <td>Debian: <em>traceroute</em>, <em>whois</em></td>
      <td><em>gw_ip_addr</em>, <em>ping_ct</em></td>
    </tr>
    <tr>
      <td><a href="scripts/sec_cp/">Secure Copy</a></td>
      <td>Securely copy file system objects to and from a server.</td>
      <td>Debian: <em>openssh-client</em></td>
      <td><em>server_options</em></td>
    </tr>
    <tr>
      <td><a href="scripts/sys_info/">System Information</a></td>
      <td>Display system overview information.</td>
      <td>Debian: <em>lshw</em>, <em>wget</em><br />Fedora: <em>lshw</em>, <em>redhat-lsb-core</em></td>
      <td><em>interface</em></td>
    </tr>
    <tr>
      <td><a href="scripts/sys_logs/">System Logs</a></td>
      <td>Display system log configuration and auditing options.</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="scripts/users_groups/">Users and Groups</a></td>
      <td>Manage users and groups.</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><a href="scripts/wget_dl/">Wget Download</a></td>
      <td>Download content with GNU Wget.</td>
      <td></td>
      <td><em>down_dir</em></td>
    </tr>
  </tbody>
</table>

### Variables to Set Command

Replace `ex_*` placeholders and run:

```console
$ find \
    "${HOME}/shell-projects/scripts" \
    -name '*.bash' \
    -exec sed -i \
    -e 's/ves_dir=""/ves_dir="ex_dir"/g' \
    -e 's/script_editor=""/script_editor="ex_ed"/g' \
    -e 's/script_editor='\'''\''/script_editor='\''ex_ed'\''/g' \
    -e 's/o_srch_type='\'''\''/o_srch_type='\''ex_type'\''/g' \
    -e 's/srch_dir=""/srch_dir="ex_dir"/g' \
    -e 's/o_req_dir=""/o_req_dir="ex_dir"/g' \
    -e 's/scripts_dir=""/scripts_dir="ex_dir"/g' \
    -e 's/dump_dir=""/dump_dir="ex_dir"/g' \
    -e 's/gw_ip_addr='\'''\''/gw_ip_addr='\''ex_addr'\''/g' \
    -e 's/ping_ct='\'''\''/ping_ct='\''ex_ct'\''/g' \
    -e 's/interface='\'''\''/interface='\''ex_itf'\''/g' \
    -e 's/down_dir=""/down_dir="ex_dir"/g' \
    {} +
```

The variables to set command **does not** include array variables.

## Templates

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="templates/">Templates</a></td>
      <td>Shell script templates.</td>
    </tr>
  </tbody>
</table>

## Project Avatar

`logo.png` is [Antu bash.svg](https://commons.wikimedia.org/wiki/File:Antu_bash.svg) by Fabián Alexis and is licensed under a [Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.