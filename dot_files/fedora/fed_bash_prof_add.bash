
#############
# Additions #
#############
# Add locations to the PATH variable
PATH="${PATH}:${HOME}/Scripts"

# Set favorite text editor
export EDITOR='vim'

# Set IDE editor
export EDITOR_IDE='vim'

# Set editor for sudoedit command
export SUDO_EDITOR='vim'

# Set Python startup file location
py_su_file="${HOME}/venv_projs/py_projs/pythonstartup.py"
if [[ -f "${py_su_file}" ]]; then
    export PYTHONSTARTUP="${py_su_file}"
fi