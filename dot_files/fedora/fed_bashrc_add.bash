
#############
# Additions #
#############
# Command Prompt Customization
if [[ -f "${HOME}/Scripts/git-prompt.sh" ]]; then
    PS1='[\u@\h \W]$(__git_ps1)\n\$ '
else
    PS1='[\u@\h \W]\n\$ '
fi

###########
# Aliases #
###########
alias a_sh_dot_files='"${EDITOR_IDE}" .bash_profile .bashrc .commonrc'
alias a_sys_get_upds='dnf check-update && flatpak remote-ls --updates --user'

#############
# Functions #
#############
f_sys_upg() {
    {
        sudo dnf upgrade --refresh -y &&
            sudo dnf autoremove -y &&
            sudo dnf clean all -y &&
            flatpak update --user -y &&
            pipx upgrade-all
    } 2>&1 | tee '/var/tmp/up.txt'

    chmod 700 '/var/tmp/up.txt'
}


# Load common customizations
. "${HOME}/.commonrc"