
#############
# Additions #
#############
# Command Prompt Customization
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:'
if [[ -f "${HOME}/Scripts/git-prompt.sh" ]]; then
    PS1+='\[\033[01;34m\]\w\[\033[00m\]$(__git_ps1)\n\$ '
else
    PS1+='\[\033[01;34m\]\w\[\033[00m\]\n\$ '
fi

###########
# Aliases #
###########
alias a_sh_dot_files='"${EDITOR_IDE}" .profile .bashrc .commonrc'
alias a_sys_get_upds='sudo apt-get update && apt list --upgradeable &&
    flatpak remote-ls --updates --user'


#############
# Functions #
#############
f_sys_upg() {
    {
        sudo apt-get update &&
            sudo apt-get upgrade -y &&
            sudo apt-get autopurge -y &&
            sudo apt-get autoclean -y &&
            flatpak update --user -y &&
            pipx upgrade-all
    } 2>&1 | tee '/var/tmp/up.txt'

    chmod 700 '/var/tmp/up.txt'
}


# Load common customizations
. "${HOME}/.commonrc"