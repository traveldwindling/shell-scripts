# Dot Files

Shell configuration dot files.

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="commonrc.bash"><code>commonrc.bash</code></a></td>
      <td>Configuration common to both Debian and Fedora.</td>
    </tr>
    <tr>
      <td><a href="debian/deb_bashrc_add.bash"><code>deb_bashrc_add.bash</code></a></td>
      <td>Debian <code>.bashrc</code> additions.</td>
    </tr>
    <tr>
      <td><a href="debian/deb_prof_additions.bash"><code>deb_prof_additions.bash</code></a></td>
      <td>Debian <code>.profile</code> additions.</td>
    </tr>
    <tr>
      <td><a href="fedora/fed_bash_prof_add.bash"><code>fed_bash_prof_add.bash</code></a></td>
      <td>Fedora <code>.bash_profile</code> additions.</td>
    </tr>
    <tr>
      <td><a href="fedora/fed_bashrc_add.bash"><code>fed_bashrc_add.bash</code></a></td>
      <td>Fedora <code>.bashrc</code> additions.</td>
    </tr>
  </tbody>
</table>

## Configuration Commands

```console
$ pwd
/home/gnu/shell-projects/dot_files
```

### Debian

```console
cat 'debian/deb_prof_additions.bash' >> "${HOME}/.profile" &&
    cat 'debian/deb_bashrc_add.bash' >> "${HOME}/.bashrc" &&
    cp 'commonrc.bash' "${HOME}/.commonrc"
```

### Fedora

```console
cat 'fedora/fed_bash_prof_add.bash' >> "${HOME}/.bash_profile" &&
    cat 'fedora/fed_bashrc_add.bash' >> "${HOME}/.bashrc" &&
    cp 'commonrc.bash' "${HOME}/.commonrc"
```