# If using an interactive shell, disable XON/XOFF flow control
# Allows for forward search of command history
[[ ${-} == *i* ]] && stty -ixon

# Command history modifications
HISTSIZE=10000
HISTFILESIZE=1000000
HISTTIMEFORMAT='%b %d %I:%M %p '
HISTIGNORE='history:h:pwd:exit:df:ls:ll'

# Load git-prompt.sh script
gp_script="${HOME}/Scripts/git-prompt.sh"
if [[ -f "${gp_script}" ]]; then
    . "${gp_script}"
fi
# https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh

# Set colors for grep --color= command
export GREP_COLORS='ms=1;31'

# Set colors for ls --color= command
LS_COLORS='di=1;33:ex=1;31'

###########
# Aliases #
###########
# Apps
alias a_apps="(ex_app_1 > '/dev/null' 2>&1 &) && 
    (ex_app_2 > '/dev/null' 2>&1 &)"

# Command Redefinitions
alias diff='diff --color=always -u'
alias grep='grep -E --color=always'
alias less='less -MR'
alias ls='ls --color=always'
alias lsblk='lsblk -o NAME,PATH,MOUNTPOINT,TYPE,SIZE'
alias pgrep='pgrep -ai'
alias tree='tree --charset=ascii'
alias vi='/usr/bin/vim'
alias xo='xdg-open'

# Date/Time
alias a_dt_stamp="date +'%Y-%m-%d_%H:%M' | tr -d '\n' | wl-copy"

# Directories
alias a_dirs_desk='cd "${HOME}/Desktop"'
alias a_dirs_doc='cd "${HOME}/Documents"'
alias a_dirs_down='cd "${HOME}/Downloads"'
alias a_dirs_scripts='cd "${HOME}/Scripts"'
alias a_dirs_ves='cd "${HOME}/venvs"'
alias a_dirs_vt='cd '\''/var/tmp'\'''

## Files
alias a_fls_full_paths='find "$(pwd)" -maxdepth 1'
alias a_fls_lint_bash='shellcheck "$(wl-paste)" | less'
alias a_fls_lint_js='jslint "$(wl-paste)" | less'
alias a_fls_lint_python='flake8 "$(wl-paste)" | less'
alias a_fls_script_perms='chmod -R 700 "${HOME}/Scripts" \
    "${HOME}/venv_projs/py_projs"'
alias a_fls_scripts='ls "${HOME}/Scripts" |
    grep '\''\.(bash|sh)'\'' | less'
alias a_fls_sort_by_s='du -ah -d1 . | sort -hr | less'

# JavaScript
alias node='node -e "require('\''repl'\'').start({ignoreUndefined: true})"'

# Networking
alias a_net_conn='ss -aptu | less'
alias a_net_gcc_net='gnome-control-center network &'
alias a_net_lip='hostname -I'
alias a_net_noff='nmcli networking off'
alias a_net_non='nmcli networking on'
alias a_net_wifi_off='nmcli radio wifi off'
alias a_net_wifi_on='nmcli radio wifi on'

# Processes
alias a_p_info='ps -eo comm,pid,%cpu,%mem,cls,nice,pri,ruser,euser \
    --sort=-%cpu | less'

# Python
alias a_py_frz_top_deps='pipdeptree \
    -f --python "$(command -v python3)" --warn silence |
    grep --color=never -E '\''^\w+'\'''
alias a_py_frz_top_deps_unp='pipdeptree \
    -f --python "$(command -v python3)" --warn silence |
    grep --color=never -E -o '\''^\w+'\'''
alias pa='. "${HOME}/venvs/py_prog/bin/activate" &&
    cd "${HOME}/venv_projs/py_projs/"'
alias pd='deactivate && cd'
alias pm='python3 -m'

# Shell
alias a_sh_aliases='alias | less'
alias a_sh_env_vars='env | sort | less'
alias a_sh_func_names='declare -F | less'
alias a_sh_internal_cmds='help | less'
alias a_sh_pretty_path='echo "${PATH}" | tr '\'':'\'' '\''\n'\'''
alias a_sh_opts='help set | less'
alias a_sh_vars_and_funcs='set | less'
alias a_sh_var_types='help declare | less'

# System
alias a_sys_cpu="cat '/proc/cpuinfo' | less"
alias a_sys_fs_info='df -Th'
alias a_sys_fwgh='sudo fwupdmgr get-history'
alias a_sys_fwgu='sudo fwupdmgr refresh --force &&
    sudo fwupdmgr get-updates'
alias a_sys_fwsar='sudo fwupdmgr --show-all'
alias a_sys_fwu='sudo fwupdmgr update'
alias a_sys_kernel='uname -r'
alias a_sys_part_tbls='sudo fdisk -l'
alias a_sys_shells='ls -l $(cat '\''/etc/shells'\'') 2> '\''/dev/null'\'''
alias a_sys_spu='echo "($(uptime |
    awk -F , '\''{ print $(NF - 1) }'\'') / $(nproc)) * 100" | bc -l |
    awk '\''{ printf("%.2f\n", $1) }'\'''


#############
# Functions #
#############
# Display file hard links
f_fls_dsp_hlinks() {
    ab_path_regex='^\/.*[^'\''"]*$'  # Set absolute path regular expression

    if [[ "${#}" -ne 2 ]]; then
        err_msg='\nThis function requires two arguments:\n\n1. A directory '
        err_msg+='tree to search\n2. A file to find hard links for'
        echo -e "${err_msg}" 1>&2

        return 1
    elif [[ "${1}" =~ ${ab_path_regex} ]]; then
        find "${1}" \
            -type d \( -path '/proc' -o -path '/run' \) -prune -o \
            -samefile "${2}" -print 2>&1 | grep -v "Permission denied|${2}"
    else
        err_msg='\nSpecify search directory tree as an absolute path.'
        echo -e "${err_msg}" 1>&2

        return 1
    fi
}


# Perform deep target scan
f_net_deep_scan() {
    sudo nmap -A "${1}" | less
}


# Ping scan a network for all available IP addresses
f_net_ping_scan() {
    nmap -Pn -sn "${1}" | less
}


# Scan 1000 TCP ports of a network
f_net_tcp_scan() {
    nmap "${1}" | less
}


# Freeze a Python environment
f_py_freeze() {
    if [[ -n "${1}" && ! -d "${1}" ]]; then
        python3 -m pip freeze |
            grep -v 'pkg_resources' > "${1}"
    else
        err_msg='\nProvide an output file path as an argument '
        err_msg+='and try again.'
        echo -e "${err_msg}" 1>&2

        return 1
    fi
}


# Serve a web directory
f_py_serve() {
    if [[ -d "${1}" ]]; then
        python3 -m http.server -d "${1}"
    else
        err_msg='\nInvalid directory path. Try again.'
        echo -e "${err_msg}" 1>&2

        return 1
    fi
}


# Display custom functions
f_sh_functions() {
    func_file="${HOME}/.commonrc"  # Set functions file

    # Obtain starting line number for functions heading
    line_num="$(\grep -m 1 -n '# Functions' "${func_file}" |
        cut -d: -f 1)"
    # Display functions line to end of file
    tail -n +"${line_num}" "${func_file}" | less
}


# Open linter error guides
f_sites_linter_guides() {
    sites=(
        'https://flake8.pycqa.org/en/latest/user/error-codes.html'
        'https://pycodestyle.pycqa.org/en/latest/intro.html#error-codes'
        'https://www.flake8rules.com/'
        'https://gist.github.com/nicerobot/53cee11ee0abbdc997661e65b348f375'
    )

    # If environment is a graphical environment
    if [[ "${XDG_CURRENT_DESKTOP}" ]]; then
        # Read out the contents of array, pipe the output to xargs,
        # and have xargs pass one URL at a time to xdg-open
        echo "${sites[@]}" | xargs -n 1 xdg-open
    else
        err_msg='\nA desktop environment must be available to '
        err_msg+='use this function.'
        echo -e "${err_msg}" 1>&2 

        return 1
    fi
}


# Open validation tools
f_sites_val() {
    sites=(
        'https://validatejavascript.com/'
        'https://jigsaw.w3.org/css-validator/#validate_by_input'
        'https://validator.w3.org/#validate_by_input'
    )

    echo "${sites[@]}" | xargs -n 1 xdg-open
}