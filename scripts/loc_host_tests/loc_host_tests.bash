#!/usr/bin/env bash

###########
# Aliases #
###########
shopt -s expand_aliases  # Enable alias expansion
alias less='less -Mr'  # Set less options
alias nmcli='nmcli -c yes'  # Set nmcli options

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
fr="$(tput sgr0)" ; readonly fr

script_name="${0##/*/}"  # Obtain script name
# Remove suffix from script name
script_name="${script_name%.*}" ; readonly script_name
# Set datetime stamp
dt_stamp="$(date +'%Y-%m-%d_%H%M')" ; readonly dt_stamp

# Define scripts directory
scripts_dir="" ; readonly scripts_dir

# Define tcpdump directory
dump_dir="" ; readonly dump_dir
# Define tcpdump path
dump_path="${dump_dir}/${dt_stamp}_dump.pcap" ; readonly dump_path

# Define menu characters and title
sep_char='*' ; readonly sep_char
menu_ti='Local Host Network Tests' ; readonly menu_ti
und_char='~' ; readonly und_char

# Calculate menu length and generate decoration strings
mt_len="${#menu_ti}" ; readonly mt_len
sep_len="$(( mt_len + 5 ))" ; readonly sep_len
und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( mt_len ))"))"
readonly und_str
sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( sep_len ))"))"
readonly sep_str

# Define menu string
menu_str="
${sep_str}

${menu_ti}
${und_str}
 1) Display NetworkManager Status and Permissions
 2) Display Network Devices
 3) Display in-memory and on-disk Connection Profiles
 4) Restart Network Services / Flush DNS Cache
 5) Display Routing Table
 6) View/Edit /etc/hosts
 7) View /etc/resolv.conf
 8) View /etc/nsswitch.conf
 9) Obtain New DHCP Leases
10) Display Established TCP/UDP Connections
11) Dump Network Traffic
12) Test Internet Speed
 q) Quit

Enter menu selection:\
" ; readonly menu_str

# Create required commands array
req_cmds=(
    'tcpdump'
    'speedtest-cli'
    'estab_conn.bash'
) ; readonly req_cmds

# Create required variables array
req_vars=(
    'dump_dir'
    'scripts_dir'
) ; readonly req_vars


#############
# Functions #
#############
#######################################
# Display script usage.
# Globals:
#   fb
#   fr
#   script_name
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: '${0}'

Purpose: Provide a local host network test battery.

${fb}tcpdump${fr} saves its output to /tmp/${script_name}/.

Requires:
    speedtest-cli
        Command line interface for testing internet bandwidth using
        speedtest.net
    tcpdump
        dump traffic on a network
    estab_conn.bash
        Display established connection information.

Variables to set:
    dump_dir
        tcpdump output file directory.
    scripts_dir
        Scripts directory.

Options:
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o h \
        --long help \
        -- "${@}")"

    # Check for invalid entries
    inv_ents="${?}" ; readonly inv_ents
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   fb
#   fr
#   req_cmds
#   req_vars
#   scripts_dir
#######################################
run_init_cks() {
    local err_msg mis_cmds req_cmd req_var unset_vars

    # Create missing commands array
    declare -a mis_cmds
    # Verify that required commands are installed
    for req_cmd in "${req_cmds[@]}"; do
        if ! command -v "${req_cmd}" > '/dev/null' 2>&1; then
            mis_cmds+=("${req_cmd}")
        fi
    done
    # Exit script if commands are missing
    if [[ "${mis_cmds[*]}" ]]; then
        err_msg='\nInstall the following command(s) before '
        err_msg+='running the script again:'
        echo -e "${err_msg}\n${fb}${mis_cmds[*]}${fr}" 1>&2
        exit 1
    fi

    # Create unset variables array
    declare -a unset_vars
    # Verify that required variables are set
    for req_var in "${req_vars[@]}"; do
        if ! [[ "${!req_var}" ]]; then
            unset_vars+=("${req_var}")
        fi
    done
    # Exit script if variables are not set
    if [[ "${unset_vars[*]}" ]]; then
        err_msg='\nSet the following variable(s) before '
        err_msg+='running the script again:'
        echo -e "${err_msg}\n${fb}${unset_vars[*]}${fr}" 1>&2
        exit 1
    fi

    # Ensure that scripts_dir is part of PATH
    if ! [[ "${PATH}" =~ .*${scripts_dir}.* ]]; then
        err_msg="\nAdd ${fb}scripts_dir${fr} to the ${fb}PATH${fr} variable "
        err_msg+='and run the script again.'
        echo -e "${err_msg}" 1>&2
        exit 1
    fi
}


#######################################
# Present menu.
# Globals:
#   dump_dir
#   dump_path
#   fb
#   fr
#   menu_str
#   scripts_dir
#######################################
menu() {
    local choice err_msg fp_msg menu_answer

    echo "${menu_str}"  # Display menu options

    read -r menu_answer  # Save input

    # Run commands that correspond to menu selection
    case "${menu_answer}" in
        '1')
            nmcli general | less

            menu
            ;;
        '2')
            nmcli -p device show

            menu
            ;;
        '3')
            nmcli connection show | less

            menu
            ;;
        '4')
            sudo systemctl restart NetworkManager.service

            menu
            ;;
        '5')
            ip route show | less

            menu
            ;;
        '6')
            while true; do
                # Prompt for view/edit choice
                echo''
                read -p 'View (v) or edit (e) /etc/hosts? ' -r choice

                if [[ "${choice}" == 'v' || "${choice}" == 'e' ]]; then
                    break
                elif [[ "${choice}" == 'q' ]]; then
                    exit 0
                else
                    err_msg='\nInvalid input. Try again or enter q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            if [[ "${choice}" == 'v' ]]; then
                less '/etc/hosts'  # View /etc/hosts
            elif [[ "${choice}" == 'e' ]]; then
                sudoedit '/etc/hosts'  # Edit /etc/hosts
            fi

            menu
            ;;
        '7')
            less '/etc/resolv.conf'

            menu
            ;;
        '8')
            less '/etc/nsswitch.conf'

            menu
            ;;
        '9')
            sudo dhclient -r && sudo dhclient

            menu
            ;;
        '10')
            "${scripts_dir}/estab_conn.bash"

            menu
            ;;
        '11')
            # If script tcpdump directory does not exist, create it
            if ! [[ -d "${dump_dir}" ]]; then
                mkdir -p "${dump_dir}"
            fi

            echo ''
            sudo tcpdump -w "${dump_path}"
            # Change tcpdump file ownership and permissions
            sudo chown "${USER}":"${USER}" "${dump_path}" &&
                chmod 700 "${dump_path}"
            tcpdump -r "${dump_path}" | less

            # Display tcpdump file path
            fp_msg="${fb}tcpdump${fr} file: ${dump_path}"
            echo "${fp_msg}" | less

            menu
            ;;
        '12')
            echo ''
            speedtest-cli --secure | less

            menu
            ;;
        'q')
            :
            ;;
        *)
            echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2

            menu
            ;;
    esac
}


#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    parse_args "${@}"

    run_init_cks

    menu  # Call menu function
}


###########
# Program #
###########
main "${@}"  # Start program
