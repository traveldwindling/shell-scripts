# Local Host Tests

Provide a local host network test battery.

## Sample Output

```console

*****************************

Local Host Network Tests
~~~~~~~~~~~~~~~~~~~~~~~~
 1) Display Network Devices
 2) Restart Network Services / Flush DNS Cache
 3) Display Routing Table
 4) View/Edit /etc/hosts
 5) View /etc/resolv.conf
 6) View /etc/nsswitch.conf
 7) Obtain new DHCP Leases
 8) Display Network Connections
 9) Dump Network Traffic
10) Test Internet Speed
 q) Quit

Enter menu selection:

```

## Required Packages/Scripts

### Debian

```console
# apt install \
    speedtest-cli \
    tcpdump
```

### Fedora

`# dnf install speedtest-cli`

### Common

[estab_conn.bash](estab_conn)

## Variables to Set

- `dump_dir` - `tcpdump` output file directory.
- `scripts_dir` - Scripts directory.