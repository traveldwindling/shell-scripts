#!/usr/bin/env bash

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
fr="$(tput sgr0)" ; readonly fr

# Set output directory for virtual environments' .txt files
if grep -iq 'debian' '/etc/os-release'; then
    o_req_dir="" ; readonly o_req_dir
elif grep -iq 'fedora' '/etc/os-release'; then
    o_req_dir="" ; readonly o_req_dir
else
    err_msg='\nUnable to verify supported GNU/Linux '
    err_msg+='distribution.'
    echo -e "${err_msg}" 1>&2
    exit 1
fi
# Set virtual environments directory
ves_dir="" ; readonly ves_dir

# Define directories to filter out from the virtual environments directory
ves_dir_fltr=() ; readonly ves_dir_fltr

# Create required variables array
req_vars=(
    'o_req_dir'
    'ves_dir'
) ; readonly req_vars


#############
# Functions #
#############
#######################################
# Display script usage.
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: '${0}'

Purpose: Freeze packages for Python virtual environments.

Requires:
    python3-pip
        pip is the Python package installer. This is the Python 3 version
        of the package.

Variables to set:
    o_req_dir
        The output directory to save Python virtual environment
        requirements files to.
    ves_dir
        The directory that contains the Python virtual environments.
    ves_dir_fltr (optional)
        Directory names to filter from the virtual environments directory.

Options:
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o h \
        --long help \
        -- "${@}")"

    # Check for invalid entries
    inv_ents="${?}" ; readonly inv_ents
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   fb
#   fr
#   req_vars
#######################################
run_init_cks() {
    local err_msg req_var unset_vars

    # Verify that module is installed
    if ! python3 -c 'import pip' > '/dev/null' 2>&1; then
        # Prompt user to install module and exit
        err_msg="\nInstall the Python module ${fb}pip${fr} before running "
        err_msg+='the script again.'
        echo -e "${err_msg}" 1>&2
        exit 1
    fi

    # Create unset variables array
    declare -a unset_vars
    # Verify that required variables are set
    for req_var in "${req_vars[@]}"; do
        if ! [[ "${!req_var}" ]]; then
            unset_vars+=("${req_var}")
        fi
    done
    # Exit script if variables are not set
    if [[ "${unset_vars[*]}" ]]; then
        err_msg='\nSet the following variable(s) before '
        err_msg+='running the script again:'
        echo -e "${err_msg}\n${fb}${unset_vars[*]}${fr}" 1>&2
        exit 1
    fi
}


#######################################
# Filter an array.
# Arguments:
#   Directories to remove.
#   Array to filter.
#######################################
fltr_array() {
    # Create local named references
    local -n dirs_to_rm="${1}"
    local -n arr_to_fltr="${2}"

    local dir_to_rm idx

    for dir_to_rm in "${dirs_to_rm[@]}"; do
        for idx in "${!arr_to_fltr[@]}"; do
            # If directory to remove is in the array to filter, remove it
            if [[ "${arr_to_fltr[${idx}]}" == "${dir_to_rm}" ]]; then
                unset 'arr_to_fltr[idx]'
            fi
        done
    done
}


#######################################
# Define starting point for execution of the program.
# Globals:
#   o_req_dir
#   ves_dir
#   ves_dir_fltr
# Arguments:
#   All script arguments.
#######################################
main() {
    local ve ve_path ves

    parse_args "${@}"

    run_init_cks

    # Declare virtual environments indexed array
    declare -a ves
    # Define virtual environments array
    mapfile -t ves < \
        <(find "${ves_dir}" -mindepth 1 -maxdepth 1 -type d -printf '%f\n')
    # Filter virtual environments array
    if [[ "${#ves_dir_fltr[@]}" -ne 0 ]]; then
        fltr_array ves_dir_fltr ves
    fi

    for ve in "${ves[@]}"; do
        # Define virtual environment path
        ve_path="${ves_dir}/${ve}"

        # Freeze packages for virtual environment
        # shellcheck disable=SC1091
        . "${ve_path}/bin/activate" &&
            python3 -m pip freeze |
                grep -v 'pkg_resources' > "${o_req_dir}/${ve}.txt" &&
            deactivate
    done
}


###########
# Program #
###########
main "${@}"  # Start program
