# Freeze Virtual Environment Packages

Freeze packages for Python [virtual environments](https://docs.python.org/3/glossary.html#term-virtual-environment).

## Required Packages/Scripts

### Debian

`# apt install python3-pip`

### Fedora

`# dnf install python3-pip`

## Variables to Set

- `o_req_dir` - The output directory to save Python virtual environment requirements files to.
- `ves_dir` - The directory that contains the Python virtual environments.
- `ves_dir_fltr` (optional) - Directories to filter from the virtual environments directory.

    For example:

    ```console
    $ tree -d -L 1 "${HOME}/venvs/"
    /home/vic_sage/venvs
    ├── virtual_env_1
    ├── virtual_env_2
    └── virtual_env_3
    ```