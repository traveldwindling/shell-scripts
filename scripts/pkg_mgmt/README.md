# Package Management

Assist with package management tasks.

## Sample Output

```console

***********************

Package Management
~~~~~~~~~~~~~~~~~~
1) View/Edit Low-Level Configuration
2) List All Local Database Packages
3) Find Local Package Associated With a File(s)
4) Search Local Database For a String
5) Display Local Package Information/Dependencies
6) View/Edit High-Level Configuration
7) View Trusted Keys
8) Remove A Trusted Key
q) Quit

Enter menu selection:

```