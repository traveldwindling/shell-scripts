#!/usr/bin/env bash
# shellcheck disable=SC2086
# Purpose: Assist with package management tasks.

###########
# Aliases #
###########
shopt -s expand_aliases  # Enable alias expansion
alias less='less -Mr'  # Set less options

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
fr="$(tput sgr0)" ; readonly fr

ter_emu='gnome-terminal' ; readonly ter_emu  # Set script terminal emulator

# Define absolute path regular expression
ab_path_regex='^\/.*[^'\''"]*$' ; readonly ab_path_regex

# Define menu characters and title
sep_char='*' ; readonly sep_char
menu_ti='Package Management' ; readonly menu_ti
und_char='~' ; readonly und_char

# Calculate menu length and generate decoration strings
mt_len="${#menu_ti}" ; readonly mt_len
sep_len="$(( mt_len + 5 ))" ; readonly sep_len
und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( mt_len ))"))"
readonly und_str
sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( sep_len ))"))"
readonly sep_str

# Define menu string
menu_str="
${sep_str}

${menu_ti}
${und_str}
1) View/Edit Low-Level Configuration
2) List All Local Database Package
3) Find Local Package Associated With a File(s)
4) Search Local Database For a String
5) Display Local Package Information/Dependencies
6) View/Edit High-Level Configuration
7) View Trusted Keys
8) Remove A Trusted Key
q) Quit

Enter menu selection:\
" ; readonly menu_str


#############
# Functions #
#############
#######################################
# Verify path is absolute.
# Globals:
#   ab_path_regex
# Arguments:
#   Absolute path to validate.
#######################################
val_abs_path() {
    local to_test
    to_test="${1}"
    [[ "${to_test}" =~ ${ab_path_regex} ]] ; echo "${?}"
}


#######################################
# Present menu.
# Globals:
#   fb
#   fr
#   ter_emu
#######################################
menu() {
    local choice dir_1 dir_2 err_msg filename_s hdls_msg key_id menu_answer \
        pass_or_fail pkg_name_s prompt search_string

    echo "${menu_str}"  # Display menu options

    read -r menu_answer  # Save input

    if grep -iq 'debian' '/etc/os-release'; then
        # Run command(s) that correspond to menu choice
        case "${menu_answer}" in
            '1')
                prompt='View (v) or edit (e) low-level config? '
                while true; do
                    # Prompt for view/edit choice
                    echo''
                    read -p "${prompt}" -r choice

                    if [[ "${choice}" == 'v' || "${choice}" == 'e' ]]; then
                        break
                    elif [[ "${choice}" == 'q' ]]; then
                        exit 0
                    else
                        err_msg='\nInvalid input. Try again or enter '
                        err_msg+='q to quit.'
                        echo -e "${err_msg}" 1>&2
                    fi
                done

                if [[ "${choice}" == 'v' ]]; then
                    less '/etc/dpkg/dpkg.cfg'
                elif [[ "${choice}" == 'e' ]]; then
                    sudoedit '/etc/dpkg/dpkg.cfg'
                fi

                menu
                ;;
            '2')
                dpkg -l | less

                menu
                ;;
            '3')
                while true; do
                    # Prompt for input, save to variable
                    echo ''
                    read -p 'Enter absolute file path(s): ' -r filename_s

                    # Validate input
                    pass_or_fail="$(val_abs_path "${filename_s}")"
                    if [[ "${filename_s}" == 'q' ]]; then
                        exit 0
                    elif [[ "${pass_or_fail}" -eq 0 ]]; then
                        break
                    else
                        err_msg='\nNot an absolute path. Try again '
                        err_msg+='or enter q to quit.'
                        echo -e "${err_msg}" 1>&2
                    fi
                done

                dpkg -S ${filename_s} | less

                menu
                ;;
            '4')
                echo ''
                read -p 'Enter string: ' -r search_string

                dpkg -l | grep -E "${search_string}" | less

                menu
                ;;
            '5')
                echo ''
                read -p 'Enter package name(s): ' -r pkg_name_s

                dpkg -s ${pkg_name_s} | less

                menu
                ;;
            '6')
                prompt='View (v) or edit (e) high-level config? '
                while true; do
                    echo''
                    read -p "${prompt}" -r choice

                    if [[ "${choice}" == 'v' || "${choice}" == 'e' ]]; then
                        break
                    elif [[ "${choice}" == 'q' ]]; then
                        exit 0
                    else
                        err_msg='\nInvalid input. Try again or enter '
                        err_msg+='q to quit.'
                        echo -e "${err_msg}" 1>&2
                    fi
                done

                # If terminal emulator is installed and using
                # a graphical environment
                if command -v "${ter_emu}" > '/dev/null' &&
                        [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                    dir_1='/etc/apt/apt.conf.d/'
                    dir_2='/etc/apt/sources.list.d/'
                    "${ter_emu}" "--working-directory=${dir_1}" \
                            > '/dev/null' 2>&1
                    "${ter_emu}" "--working-directory=${dir_2}" \
                            > '/dev/null' 2>&1
                else
                    hdls_msg="\nReview ${fb}/etc/apt/apt.conf.d/${fr} and "
                    hdls_msg+="${fb}/etc/apt/sources.list.d/${fr} for "
                    hdls_msg+='additional high-level configuration '
                    hdls_msg+='information.'
                    echo -e "${hdls_msg}" | less
                fi

                if [[ "${choice}" == 'v' ]]; then
                    less '/etc/apt/sources.list'
                elif [[ "${choice}" == 'e' ]]; then
                    sudoedit '/etc/apt/sources.list'
                fi

                menu
                ;;
            '7')
                apt-key list | less

                menu
                ;;
            '8')
                echo ''
                read -p 'Enter key ID: ' -r key_id
                key_id="${key_id//[^a-zA-Z0-9 -]/}"

                sudo apt-key del "${key_id}" | less

                menu
                ;;
            'q')
                :
                ;;
            *)
                echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2

                menu
                ;;
        esac
    elif grep -iq 'fedora' '/etc/os-release'; then
        case "${menu_answer}" in
            '1')
                prompt='View (v) or edit (e) low-level config? '
                while true; do
                    echo''
                    read -p "${prompt}" -r choice

                    if [[ "${choice}" == 'v' || "${choice}" == 'e' ]]; then
                        break
                    elif [[ "${choice}" == 'q' ]]; then
                        exit 0
                    else
                        err_msg='\nInvalid input. Try again or enter '
                        err_msg+='q to quit.'
                        echo -e "${err_msg}" 1>&2
                    fi
                done

                if command -v "${ter_emu}" > '/dev/null' &&
                        [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                    dir_1='/usr/lib/rpm/'
                    "${ter_emu}" "--working-directory=${dir_1}" \
                        > '/dev/null' 2>&1
                else
                    hdls_msg="\nReview ${fb}/usr/lib/rpm/${fr} for "
                    hdls_msg+='additional low-level configuration '
                    hdls_msg+='information.'
                    echo -e "${hdls_msg}" | less
                fi

                if [[ "${choice}" == 'v' ]]; then
                    find '/usr/lib/rpm/' -maxdepth 0 -exec ls -hl '{}' \; | 
                        less
                fi

                menu
                ;;
            '2')
                rpm -q -a | sort | less

                menu
                ;;
            '3')
                while true; do
                    # Prompt for input, save to variable
                    echo ''
                    read -p 'Enter absolute file path(s): ' -r filename_s

                    # Validate input
                    pass_or_fail="$(val_abs_path "${filename_s}")"
                    if [[ "${filename_s}" == 'q' ]]; then
                        exit 0
                    elif [[ "${pass_or_fail}" -eq 0 ]]; then
                        break
                    else
                        err_msg='\nNot an absolute path. Try again '
                        err_msg+='or enter q to quit.'
                        echo -e "${err_msg}" 1>&2
                    fi
                done

                rpm -q -f ${filename_s} | less

                menu
                ;;
            '4')
                echo ''
                read -p 'Enter string: ' -r search_string

                rpm -q -a | grep -E "${search_string}" | less

                menu
                ;;
            '5')
                echo ''
                read -p 'Enter package name(s): ' -r pkg_name_s

                rpm -q -R ${pkg_name_s} | less

                menu
                ;;
            '6')
                prompt='View (v) or edit (e) high-level config? '
                while true; do
                    echo''
                    read -p "${prompt}" -r choice

                    if [[ "${choice}" == 'v' || "${choice}" == 'e' ]]; then
                        break
                    elif [[ "${choice}" == 'q' ]]; then
                        exit 0
                    else
                        err_msg='\nInvalid input. Try again or enter '
                        err_msg+='q to quit.'
                        echo -e "${err_msg}" 1>&2
                    fi
                done

                if command -v "${ter_emu}" > '/dev/null' &&
                        [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                    dir_1='/etc/yum.repos.d/'
                    "${ter_emu}" "--working-directory=${dir_1}" \
                        > '/dev/null' 2>&1
                else
                    hdls_msg="\nReview ${fb}/etc/yum.repos.d/${fr} for "
                    hdls_msg+='additional high-level configuration '
                    hdls_msg+='information.'
                    echo -e "${hdls_msg}" | less
                fi

                if [[ "${choice}" == 'v' ]]; then
                    less '/etc/dnf/dnf.conf'
                elif [[ "${choice}" == 'e' ]]; then
                    sudoedit '/etc/dnf/dnf.conf'
                fi

                menu
                ;;
            '7')
                rpm -q gpg-pubkey --queryformat \
                        '%{NAME}-%{VERSION}-%{RELEASE}\t%{SUMMARY}\n' | less

                menu
                ;;
            '8')
                echo ''
                read -p 'Enter key ID: ' -r key_id

                sudo rpm -e "${key_id}" | less

                menu
                ;;
            'q')
                :
                ;;
            *)
                echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2

                menu
                ;;
        esac
    else
        err_msg='\nUnable to verify supported GNU/Linux '
        err_msg+='distribution.'
        echo -e "${err_msg}" 1>&2
        exit 1
    fi
}


#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    menu  # Call menu function
}


###########
# Program #
###########
main "${@}"  # Start program
