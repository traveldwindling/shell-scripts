#!/usr/bin/env bash

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
fr="$(tput sgr0)" ; readonly fr

# Obtain script root
script_root="${BASH_SOURCE[0]%/*}" ; readonly script_root
# Obtain script name
script_name="${0##/*/}"
script_name="${script_name%.*}" ; readonly script_name

# Set log root
log_root="${script_root}/logs" ; readonly log_root
# Set log directory
log_dir="${log_root}/${script_name}" ; readonly log_dir
# Set log path
log_path="${log_dir}/${script_name}.log" ; readonly log_path
# Set max log size
max_log_size=10485760 ; readonly max_log_size
# Set maximum rolled over log number
max_roll_num=3 ; readonly max_roll_num
# Set excess rolled over log number
exc_roll_num="$(( max_roll_num + 1 ))" ; readonly exc_roll_num
log_dt_fmt="$(date +'%Y-%m-%d %H:%M:%S')" ; readonly log_dt_fmt

# Set download directory
down_dir="" ; readonly down_dir

# Define URL regular expression
url_regex='https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.'
url_regex+='[a-zA-Z0-9()]{1,18}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)'
readonly url_regex


#############
# Functions #
#############
#######################################
# Display script usage.
# Globals:
#   log_path
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: '${0}'

Purpose: Download content with GNU Wget.

A log is saved to '${log_path}'.

Variables to set:
    down_dir
        Directory to save downloaded content to.

Options:
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


#######################################
# Verify string is valid URL.
# Globals:
#   url_regex
# Arguments:
#   URL to validate.
#######################################
val_url() {
    local to_test
    to_test="${1}"
    [[ "${to_test}" =~ ${url_regex} ]] ; echo "${?}"
}


#######################################
# Set URL.
# Arguments:
#   Prompt text.
#######################################
set_url() {
    local err_msg pass_or_fail prompt

    prompt="${1}"
    while true; do
        log_msg 'DEBUG' 'Capturing input'
        read -p "${prompt}" -r url

        log_msg 'DEBUG' 'Validating input'
        pass_or_fail="$(val_url "${url}")"
        if [[ "${url}" == 'q' ]]; then
            log_msg 'DEBUG' 'Exiting program'
            exit 0
        elif [[ "${pass_or_fail}" -eq 0 ]]; then
            break
        else
            log_msg 'ERROR' "Invalid URL provided: \"${url}\""
            err_msg='\nInvalid URL. Try again or enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done
}


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o h \
        --long help \
        -- "${@}")"

    # Check for invalid entries
    inv_ents="${?}" ; readonly inv_ents
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   down_dir
#   fb
#   fr
#######################################
run_init_cks() {
    local err_msg

    # Verify that required variable is set
    if ! [[ "${down_dir}" ]]; then
        # Prompt user to set required variable and exit
        err_msg="\nSet ${fb}down_dir${fr} before running the script again."
        echo -e "${err_msg}" 1>&2
        exit 1
    fi
}


#######################################
# Handle a SIGINT signal.
#######################################
hndl_sigint() {
    # shellcheck disable=SC2317
    echo -e '\n\nSIGINT or Ctrl+c detected. Exiting gracefully.'
    # shellcheck disable=SC2317
    exit 130
}


#######################################
# Initialize logging.
# Globals:
#   exc_roll_num
#   log_dir
#   log_path
#   max_log_size
#   max_roll_num
#   script_name
#######################################
init_logging() {
    local h_log_num i log_name log_names log_nums log_size new_num num \
        roll_over

    # If script log directory does not exist, create it
    if ! [[ -d "${log_dir}" ]]; then
        mkdir -p "${log_dir}"
    fi

    # If script log file does not exist, create it
    if ! [[ -f "${log_path}" ]]; then
        touch "${log_path}"
    fi

    log_size="$(stat -c%s "${log_path}")"  # Get current log size
    # Test whether current log file needs to be rolled over
    roll_over="$([[ "${log_size}" -gt "${max_log_size}" ]] ; echo "${?}")"

    # Declare log file names indexed array
    declare -a log_names
    # Create array of log file names
    # shellcheck disable=SC2061
    # shellcheck disable=SC2086
    mapfile -t log_names < \
        <(find "${log_dir}" \
            -mindepth 1 \
            -maxdepth 1 \
            -type f \
            -name ${script_name}.log* \
            -printf '%f\n'
        )

    # If there are rolled over log files
    if [[ "${#log_names[*]}" -gt 1 ]]; then
        # Declare log file numbers indexed array
        declare -a log_nums
        for log_name in "${log_names[@]}"; do
            if [[ "${log_name}" =~ ${script_name}.log.[1-"${max_roll_num}"] ]]
            then
                log_nums+=("${log_name##*.}")
            fi
        done

        # If there are rolled over log files, find highest log file number
        if [[ "${log_nums[*]}" ]]; then
            h_log_num="${log_nums[0]}"

            for num in "${log_nums[@]}"; do
                [[ "${num}" -gt "${h_log_num}" ]] && h_log_num="${num}"
            done
        fi

        # If the current log needs to be rolled over
        if [[ "${roll_over}" -eq 0 ]]; then
            # Roll over rolled over log files
            for (( i="${h_log_num}"; i >= 1; i-- )); do
                new_num="$(( i + 1 ))"
                mv "${log_path}.${i}" "${log_path}.${new_num}"
            done
        fi

        # Remove excess rolled over file
        if [[ -f  "${log_path}.${exc_roll_num}" ]]; then
            rm "${log_path}.${exc_roll_num}"
        fi
    fi

    if [[ "${roll_over}" -eq 0 ]]; then
        mv "${log_path}" "${log_path}.1"  # Roll over current log file
        touch "${log_path}"  # Re-create current log file
    fi
}


#######################################
# Log a message.
# Globals:
#   log_dt_fmt
#   log_path
# Arguments:
#   Log level.
#   Log message.
#######################################
log_msg() {
    local log_level log_message

    log_level="${1}"
    log_message="${2}"
    echo "${log_dt_fmt} - ${log_level} - ${log_message}" >> "${log_path}"
}


#######################################
# Define starting point for execution of the program.
# Globals:
#   down_dir
#   log_path
#   url
# Arguments:
#   All script arguments.
#######################################
main() {
    local prompt

    parse_args "${@}"

    run_init_cks

    trap hndl_sigint SIGINT

    init_logging

    log_msg 'DEBUG' 'Starting program'

    # Create download directory if it does not exist
    if ! [[ -d "${down_dir}" ]]; then
        log_msg 'DEBUG' 'Creating download directory'
        mkdir "${down_dir}"
    fi

    cd "${down_dir}" || exit 1

    echo ''
    prompt='Enter URL (q to quit): '
    while true; do
        set_url "${prompt}"

        log_msg 'DEBUG' "Downloading URL \"${url}\""
        wget "${url}" 2>> "${log_path}" &
    done
}


###########
# Program #
###########
main "${@}"  # Start program
