#!/usr/bin/env bash
# Purpose: Display system log configuration and auditing options.

###########
# Aliases #
###########
shopt -s expand_aliases  # Enable alias expansion
alias less='less -Mr'  # Set less options

# Define formatting variables
fb="$(tput bold)" ; readonly fb
fr="$(tput sgr0)" ; readonly fr

# Ensure systemd and related utilities use color in their output
export SYSTEMD_COLORS='true'

# Define absolute path regular expression
ab_path_regex='^\/.*[^'\''"]*$' ; readonly ab_path_regex

# Define menu characters and title
sep_char='*' ; readonly sep_char
menu_ti='System Logs' ; readonly menu_ti
und_char='~' ; readonly und_char

# Calculate menu length and generate decoration strings
mt_len="${#menu_ti}" ; readonly mt_len
sep_len="$(( mt_len + 5 ))" ; readonly sep_len
und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( mt_len ))"))"
readonly und_str
sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( sep_len ))"))"
readonly sep_str

# Define menu string
menu_str="
${sep_str}

${menu_ti}
${und_str}
 1) View systemd Journal Config Options
 2) View/Edit systemd Journal Config
 3) View Journal Usage Space
 4) View Journal in Reverse Chronological Order
 5) View Logs for the Current Boot
 6) View List of Boot Identifier
 7) View Logs for a Specific Boot
 8) View Kernel Messages
 9) View Logs for a Specific Binary
10) View Available Unit Files
11) View Running Units
12) View Timer Units
13) View Logs for a Specific Unit(s)
14) View systemd Journal Fields
15) View Logs by Specific Field Filter(s)
16) View Logs at or Below Specific Priority Level
17) View Logs Since a Specific Date/Time (YYYY-MM-DD hh:mm:ss)
18) View Logs Until a Specific Date/Time (YYYY-MM-DD hh:mm:ss)
 q) Quit

Enter menu selection:\
" ; readonly menu_str


#############
# Functions #
#############
#######################################
# Verify path is absolute.
# Globals:
#   ab_path_regex
# Arguments:
#   Absolute path to validate.
#######################################
val_abs_path() {
    local to_test
    to_test="${1}"
    [[ "${to_test}" =~ ${ab_path_regex} ]] ; echo "${?}"
}


#######################################
# Present menu.
# Globals:
#   fb
#   fr
#   menu_str
#######################################
menu() {
    local bin_path boot_id choice err_msg field_filters log_pri menu_answer \
        pass_or_fail prompt since_dt until_dt unit_names

    echo "${menu_str}"  # Display menu options

    read -r menu_answer  # Save input

    # Run command(s) that correspond to menu selection
    case "${menu_answer}" in
        '1')
            man 5 journald.conf

            menu
            ;;
        '2')
            while true; do
                # Prompt for view/edit choice
                echo''
                prompt='View (v) or edit (e) /etc/systemd/journald.conf? '
                read -p "${prompt}" -r choice

                if [[ "${choice}" == 'v' || "${choice}" == 'e' ]]; then
                    break
                elif [[ "${choice}" == 'q' ]]; then
                    exit 0
                else
                    err_msg='\nInvalid input. Try again or enter q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            if [[ "${choice}" == 'v' ]]; then
                # View /etc/systemd/journald.conf
                less '/etc/systemd/journald.conf'
            elif [[ "${choice}" == 'e' ]]; then
                # Edit /etc/systemd/journald.conf
                sudoedit '/etc/systemd/journald.conf'
            fi

            menu
            ;;
        '3')
            journalctl --disk-usage | less

            menu
            ;;
        '4')
            journalctl -r | less

            menu
            ;;
        '5')
            journalctl -b | less

            menu
            ;;
        '6')
            journalctl --list-boots | less

            menu
            ;;
        '7')
            echo ''
            read -p 'Enter a boot identifier: ' -r boot_id
            boot_id="${boot_id//[^a-z0-9]/}"  # Sanitize input

            journalctl -b "${boot_id}" | less

            menu
            ;;
        '8')
            journalctl -k | less

            menu
            ;;
        '9')
            while true; do
                echo ''
                read -p 'Enter a binary'\''s full path: ' -r bin_path

                # Check for valid selection
                pass_or_fail="$(val_abs_path "${bin_path}")"
                if [[ "${bin_path}" == 'q' ]]; then
                    exit 0
                elif [[ "${pass_or_fail}" -eq 0 ]]; then
                    break
                else
                    err_msg='\nNot an absolute path. Try again or enter '
                    err_msg+='q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            journalctl "${bin_path}" | less

            menu
            ;;
        '10')
            systemctl list-unit-files | less

            menu
            ;;
        '11')
            systemctl list-units | less

            menu
            ;;
        '12')
            systemctl --all list-timers | less

            menu
            ;;
        '13')
            echo ''
            read -p 'Enter unit names (prepend each unit name with -u): ' \
                -r unit_names
            unit_names="${unit_names//[^a-zA-Z0-9 .\-]/}"
            # shellcheck disable=SC2086
            journalctl ${unit_names} | less

            menu
            ;;
        '14')
            man 7 systemd.journal-fields

            menu
            ;;
        '15')
            echo ''
            read -p 'Enter field filters (field_filter=value): '\
                -r field_filters
            field_filters="${field_filters//[^a-zA-Z0-9 _=\-]/}"
            # shellcheck disable=SC2086
            journalctl ${field_filters} | less

            menu
            ;;
        '16')
            echo -e '\nemerg, alert, crit, err, warn, notice, info, debug'
            read -p 'Enter priority: ' -r log_pri
            log_pri="${log_pri//[^a-z]/}"

            journalctl -p "${log_pri}" | less

            menu
            ;;
        '17')
            echo ''
            read -p 'Enter date/time (YYYY-MM-DD hh:mm:ss): ' -r since_dt
            since_dt="${since_dt//[^0-9 \-:]/}"

            journalctl --since="${since_dt}" | less

            menu
            ;;
        '18')
            echo ''
            read -p 'Enter date/time (YYYY-MM-DD hh:mm:ss): ' -r until_dt
            until_dt="${until_dt//[^0-9 \-:]/}"

            journalctl --until="${until_dt}" | less

            menu
            ;;
        'q')
            :
            ;;
        *)
            echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2

            menu
            ;;
    esac
}


#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    menu  # Call menu function
}


###########
# Program #
###########
main "${@}"  # Start program
