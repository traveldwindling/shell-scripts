# System Logs

Display system log configuration and auditing options.

## Sample Output

```console

****************

System Logs
~~~~~~~~~~~
 1) View systemd Journal Config Options
 2) Edit systemd Journal Configuration
 3) View Journal Usage Space
 4) View Journal in Reverse Chronological Order
 5) View Logs for the Current Boot
 6) View List of Boot Identifiers
 7) View Logs for a Specific Boot
 8) View Kernel Messages
 9) View Logs for a Specific Binary
10) View Available Unit Files
11) View Running Units
12) View Timer Units
13) View Logs for a Specific Unit(s)
14) View systemd Journal Fields
15) View Logs by Specific Field Filter(s)
16) View Logs at or Below Specific Priority Level
17) View Logs Since a Specific Date/Time (YYYY-MM-DD hh:mm:ss)
18) View Logs Until a Specific Date/Time (YYYY-MM-DD hh:mm:ss)
 q) Quit

Enter menu selection:

```