# Create Timer

Create new systemd timer.

## Variables to Set

- `script_editor` - Program to use for editing files.

    By default, the script will respect your environment's `EDITOR` value. If this value is not configured, you can set `script_editor` to your editor of choice.