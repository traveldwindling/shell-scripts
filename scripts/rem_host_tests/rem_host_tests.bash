#!/usr/bin/env bash

###########
# Aliases #
###########
shopt -s expand_aliases  # Enable alias expansion
alias less='less -Mr'  # Set less options

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
ft="$(tput sitm)" ; readonly ft
fr="$(tput sgr0)" ; readonly fr

gw_ip_addr='' ; readonly gw_ip_addr  # Set gateway IP address
ping_ct='' ; readonly ping_ct  # Set ping count

# Define URL regular expression
url_regex='https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.'
url_regex+='[a-zA-Z0-9()]{1,18}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)'
readonly url_regex
# Define IPv4 address regular expression
ipv4_regex='(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|'
ipv4_regex+='2[0-4][0-9]|[01]?[0-9][0-9]?)){3}'
readonly ipv4_regex

# Set resource records
res_recs=('A' 'AAAA' 'AFSDB' 'APL' 'CAA' 'CDNSKEY' 'CDS' 'CERT'
          'CNAME' 'CSYNC' 'DHCID' 'DLV' 'DNAME' 'DNSKEY' 'DS'
          'EUI48' 'EUI64' 'HINFO' 'HIP' 'HTTPS' 'IPSECKEY' 'KEY'
          'KX' 'LOC' 'MX' 'NAPTR' 'NS' 'NSEC' 'NSEC3' 'NSEC3PARAM'
          'OPENPGPKEY' 'PTR' 'RRSIG' 'RP' 'SIG' 'SMIMEA' 'SOA'
          'SRV' 'SSHFP' 'SVCB' 'TA' 'TKEY' 'TLSA' 'TSIG' 'TXT'
          'URI' 'ZONEMD') ; readonly res_recs

# Define menu characters and title
sep_char='*' ; readonly sep_char
menu_ti='Remote Host Network Tests' ; readonly menu_ti
und_char='~' ; readonly und_char

# Calculate menu length and generate decoration strings
mt_len="${#menu_ti}" ; readonly mt_len
sep_len="$(( mt_len + 5 ))" ; readonly sep_len
und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( mt_len ))"))"
readonly und_str
sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( sep_len ))"))"
readonly sep_str

# Define menu string
menu_pre="
${sep_str}

${menu_ti}
" ; readonly menu_pre

menu_post="\
${und_str}
 1) Request ICMP ECHO_RESPONSE
 2) Print Route Packets Trace
 3) WHOIS Lookup
 4) WHOIS IP Address Lookup
 5) Simple DNS Lookup
 6) Advanced DNS Lookup
 7) Request Specific DNS Record
 8) DNS Delegation Path Trace
 9) Fetch HTTP Header
10) Is It Down?
 s) Set Current URL
 q) Quit

Enter menu selection:\
" ; readonly menu_post

# Create required commands array
req_cmds=(
    'traceroute'
    'whois'
) ; readonly req_cmds

# Create required variables array
req_vars=('gw_ip_addr' 'ping_ct') ; readonly req_vars


#############
# Functions #
#############
#######################################
# Display script usage.
# Globals:
#   fb
#   fr
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: '${0}'

Purpose: Provide a remote host network test battery.

Requires:
    traceroute
        print the route packets trace to network host
    whois
        client for the whois service

Variables to set:
    gw_ip_addr
        Your network's gateway IP address.
    ping_ct
        The number of ${fb}ECHO_REQUEST${fr} packets to be sent by the
        ${fb}ping${fr} command.

Options:
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


# Validation Functions Start #
#######################################
# Verify string is valid URL.
# Globals:
#   url_regex
# Arguments:
#   URL to validate.
#######################################
val_url() {
    local to_test
    to_test="${1}"
    [[ "${to_test}" =~ ${url_regex} ]] ; echo "${?}"
}


#######################################
# Verify string is valid IPv4 address.
# Globals:
#   ipv4_regex
# Arguments:
#   IPv4 address to validate.
#######################################
val_ipv4_addr() {
    local to_test
    to_test="${1}"
    [[ "${to_test}" =~ ${ipv4_regex} ]] ; echo "${?}"
}
# Validation Functions End #


#######################################
# Obtain and set site address.
#######################################
set_address() {
    local end_char err_msg i minus_end_slash pass_or_fail prompt slcd url_len

    while true; do
        echo ''
        prompt='URL with second-level .co domain (y or n)? '
        read -p "${prompt}" -r slcd

        # Validate input
        if [[ "${slcd}" == 'y' || "${slcd}" == 'n' ]]; then
            break
        elif [[ "${slcd}" == 'q' ]]; then
            exit 0
        else
            err_msg='\nInvalid input. Try again or enter q to quit.'
            echo -e "${err_msg}" 1>&2
        fi
    done

    while true; do
        read -p 'Enter URL: ' -r url

        pass_or_fail="$(val_url "${url}")"
        if [[ "${url}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail}" -eq 0 ]]; then
            break
        else
            err_msg='\nInvalid URL. Try again or enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done

    # Determine URL length and end character, save to variables
    url_len="${#url}"
    end_char="${url:(( url_len - 1 ))}"

    period_counter=0  # Set period counter variable

    # If input ends with forward slash
    if [[ "${end_char}" == '/' ]]; then
        minus_end_slash="${url:0:(( url_len - 1 ))}"
        address="${minus_end_slash#*//}"
    # If input does not end with forward slash
    elif ! [[ "${end_char}" == '/' ]]; then
        address="${url#*//}"
    fi

    # Loop through each character of address variable
    for (( i=0; i<"${#address}"; i++ )); do
        # Determine if character is a period
        if [[ "${address:$i:1}" == '.' ]]; then
            (( period_counter++ ))
        fi
    done

    # Check number of periods for subdomain identification
    # Save address value minus the subdomain
    if [[ "${period_counter}" -ge 2 && "${slcd}" == 'n' ]]; then
        addr_ms_sub="$(echo "${address}" |
            cut \
            -d'.' \
            -f "${period_counter}-$(( period_counter + 1 ))" )"
    elif [[ "${period_counter}" -ge 2 && "${slcd}" == 'y' ]]; then
        addr_ms_sub="$(echo "${address}" |
            cut \
            -d'.' \
            -f "$(( period_counter - 1))-$(( period_counter + 1 ))" )"
    fi
}


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o h \
        --long help \
        -- "${@}")"

    # Check for invalid entries
    inv_ents="${?}" ; readonly inv_ents
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   fb
#   fr
#   req_cmds
#   req_vars
#   scripts_dir
#######################################
run_init_cks() {
    local err_msg mis_cmds req_cmd req_var unset_vars

    # Create missing commands array
    declare -a mis_cmds
    # Verify that required commands are installed
    for req_cmd in "${req_cmds[@]}"; do
        if ! command -v "${req_cmd}" > '/dev/null' 2>&1; then
            mis_cmds+=("${req_cmd}")
        fi
    done
    # Exit script if commands are missing
    if [[ "${mis_cmds[*]}" ]]; then
        err_msg='\nInstall the following command(s) before '
        err_msg+='running the script again:'
        echo -e "${err_msg}\n${fb}${mis_cmds[*]}${fr}" 1>&2
        exit 1
    fi

    # Create unset variables array
    declare -a unset_vars
    # Verify that required variables are set
    for req_var in "${req_vars[@]}"; do
        if ! [[ "${!req_var}" ]]; then
            unset_vars+=("${req_var}")
        fi
    done
    # Exit script if variables are not set
    if [[ "${unset_vars[*]}" ]]; then
        err_msg='\nSet the following variable(s) before '
        err_msg+='running the script again:'
        echo -e "${err_msg}\n${fb}${unset_vars[*]}${fr}" 1>&2
        exit 1
    fi
}


#######################################
# Present menu.
# Globals:
#   address
#   addr_ms_sub
#   fb
#   fr
#   ft
#   gw_ip_addr
#   menu_post
#   menu_pre
#   period_counter
#   ping_ct
#   res_recs
#   url
#######################################
menu() {
    local curr_addr err_msg ip_address menu_answer pass_or_fail

    if [[ "${url}" ]]; then
        curr_addr="Current Address: ${ft}${address}${fr}\n"
    fi

    # Display menu options
    echo -e "${menu_pre}${curr_addr}${menu_post}"

    read -r menu_answer  # Save input to variable

    # Run commands that correspond to menu selection
    case ${menu_answer} in
        '1')
            if ! [[ "${url}" ]]; then
                set_address
            fi

            ping -c "${ping_ct}" -n "${address}" | less

            menu
            ;;
        '2')
            if ! [[ "${url}" ]]; then
                set_address
            fi

            sudo traceroute -In "${address}" | less
            
            menu
            ;;
        '3')
            if ! [[ "${url}" ]]; then
                set_address
            fi

            if [[ ${period_counter} -ge 2 ]]; then
                whois "${addr_ms_sub}" | less
            else
                whois "${address}" | less
            fi

            menu
            ;;
        '4')
            while true; do
                echo ''
                read -p 'Enter IP address: ' -r ip_address
                
                pass_or_fail="$(val_ipv4_addr "${ip_address}")"
                if [[ "${ip_address}" == 'q' ]]; then
                    exit 0
                elif [[ "${pass_or_fail}" -eq 0 ]]; then
                    break
                else
                    err_msg='\nInvalid IP address. Try again '
                    err_msg+='or enter q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            whois "${ip_address}" | less

            menu
            ;;
        '5')
            if ! [[ "${url}" ]]; then
                set_address
            fi

            host "${address}" | less

            menu
            ;;
        '6')
            if ! [[ "${url}" ]]; then
                set_address
            fi

            dig "${address}" ANY | less

            menu
            ;;
        '7')
            if ! [[ "${url}" ]]; then
                set_address
            fi

            while true; do
                # Prompt for input, save to variable
                echo ''
                read -p 'Enter DNS record of interest: ' -r res_rec

                # Check for valid selection
                if [[ "${res_recs[*]}" =~ ${res_rec} ]]; then
                    break
                elif [[ "${res_rec}" == 'q' ]]; then
                    exit 0
                else
                    err_msg='\nInvalid input. Try again or enter q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            dig "${address}" "${res_rec}" | less

            menu
            ;;
        '8')
            if ! [[ "${url}" ]]; then
                set_address
            fi

            dig "@${gw_ip_addr}" "${address}" +trace | less

            menu
            ;;
        '9')
            if ! [[ "${url}" ]]; then
                set_address
            fi

            wget -q -S --spider "${url}" | less

            menu
            ;;
        '10')
            if ! [[ "${url}" ]]; then
                set_address
            fi

            if [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                xdg-open "https://downforeveryoneorjustme.com/${address}" \
                    > '/dev/null' 2>&1
            else
                err_msg='\nA desktop environment must be available to '
                err_msg+='use this option.'
                echo -e "${err_msg}" 1>&2
            fi

            menu
            ;;
        's')
            set_address

            menu
            ;;
        'q')
            :
            ;;
        *)
            echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2

            menu
            ;;
    esac
}


#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    parse_args "${@}"

    run_init_cks

    menu  # Call menu function
}


###########
# Program #
###########
main "${@}"  # Start program
