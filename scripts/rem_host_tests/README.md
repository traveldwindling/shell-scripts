# Remote Host Tests

Provide a remote host network test battery.

## Sample Output

```console

******************************

Remote Host Network Tests
~~~~~~~~~~~~~~~~~~~~~~~~~
 1) Request ICMP ECHO_RESPONSE
 2) Print route packets trace
 3) WHOIS lookup
 4) WHOIS IP address lookup
 5) Simple DNS lookup
 6) Advanced DNS lookup
 7) Request specific DNS record
 8) DNS delegation path trace
 9) Fetch HTTP header
10) Is it Down?
 s) Set Current URL
 q) Quit

Enter menu selection:

```

## Required Packages/Scripts

### Debian

```console
# apt install \
    traceroute \
    whois
```

## Variables to Set

- `gw_ip_addr` - Your network's gateway IP address.
- `ping_ct` - The number of `ECHO_REQUEST` packets to be sent by the `ping` command.