#!/usr/bin/env bash

###########
# Aliases #
###########
shopt -s expand_aliases  # Enable alias expansion
alias less='less -Mr'  # Set less options

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
ft="$(tput sitm)" ; readonly ft
fr="$(tput sgr0)" ; readonly fr

script_editor='' ; readonly script_editor  # Set script editor
ter_emu='gnome-terminal' ; readonly ter_emu  # Set script terminal emulator

# Declare and define directory associative array
declare -A dir_options=(
    ['Quit']=''
) ; readonly dir_options

# Define menu characters and title
sep_char='*' ; readonly sep_char
work_menu_ti='Work Menu' ; readonly work_menu_ti
und_char='~' ; readonly und_char

# Calculate menu length and generate decoration strings
work_mt_len="${#work_menu_ti}" ; readonly work_mt_len
work_sep_len="$(( work_mt_len + 5 ))" ; readonly work_sep_len
work_und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( work_mt_len ))"))"
readonly work_und_str
work_sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( work_sep_len ))"))"
readonly work_sep_str

# Define work menu strings
work_menu_pre="
${work_sep_str}

${work_menu_ti}
" ; readonly work_menu_pre

work_menu_post="\
${work_und_str}
c) Clone a Project
l) List Projects
s) Set Current Project
d) Directory Menu
q) Quit

Enter menu selection:\
" ; readonly work_menu_post

# Define menu characters and title
git_menu_ti='Git Menu' ; readonly git_menu_ti

# Calculate menu length and generate decoration strings
git_mt_len="${#git_menu_ti}" ; readonly git_mt_len
git_sep_len="$(( git_mt_len + 5 ))" ; readonly git_sep_len
git_und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( git_mt_len ))"))"
readonly git_und_str
git_sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( git_sep_len ))"))"
readonly git_sep_str

# Define Git menu strings
git_menu_pre="
${git_sep_str}

${git_menu_ti}
" ; readonly git_menu_pre

git_menu_post="\
${git_und_str}
 1) Project Status
 2) Project Log
 3) Project diff
 4) Project config
 5) View All Project Branches
 6) Create a New Branch
 7) Switch to a New Branch
 8) View Project Remotes
 9) Pull Changes
10) Push Changes
 t) Open Current Directory in New Terminal
 w) Work Menu
 q) Quit
 
Enter menu selection:\
" ; readonly git_menu_post


#############
# Functions #
#############
#######################################
# Display script usage.
# Globals:
#   fb
#   fr
#   ft
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: '${0}'

Purpose: Manage various Git-based projects.

Requires:
    git
        the content tracker

Variables to set:
    dir_options
        Associative array of directories that contain Git-based projects where
        the ${ft}key${fr} is the directory name to display in 
        ${fb}dir_menu${fr} and the ${ft}value${fr} is the absolute path to the directory.
    script_editor
        Program to use for editing files.

        By default, the script will respect your environment's ${fb}EDITOR${fr}
        value. If this value is not configured, you can set
        ${fb}script_editor${fr} to your editor of choice.

Options:
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


#######################################
# Verify object exists and is a directory.
# Arguments:
#   Extant directory to validate.
#######################################
val_extant_dir() {
    local to_test
    to_test="${1}"
    [[ -d "${to_test}" ]] ; echo "${?}"
}


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o h \
        --long help \
        -- "${@}")"

    # Check for invalid entries
    inv_ents="${?}" ; readonly inv_ents
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   dir_options
#   fb
#   fr
#   script_editor
#######################################
run_init_cks() {
    local err_msg

    # Verify that git is installed
    if ! command -v git > '/dev/null' 2>&1; then
        # Prompt user to install git and exit
        err_msg="\nInstall ${fb}git${fr} before running the script again."
        echo -e "${err_msg}" 1>&2
        exit 1
    fi

    # Override EDITOR with script_editor
    if [[ "${script_editor}" ]]; then
        EDITOR="${script_editor}"
    fi
    # Verify that an editor is set
    if ! [[ "${EDITOR}" || "${script_editor}" ]]; then
        # Prompt user to set required variable and exit
        err_msg="\nSet either ${fb}EDITOR${fr} or ${fb}script_editor${fr} "
        err_msg+='before running the script again.'
        echo -e "${err_msg}" 1>&2
        exit 1
    fi

    # Verify that dir_options array has been modified
    if [[ "${#dir_options[*]}" -eq 1 ]]; then
        err_msg='\nEnsure directories have been added to '
        err_msg+="${fb}dir_options${fr} before running the script again."
        echo -e "${err_msg}" 1>&2
        exit 1
    fi
}


#######################################
# Pause script.
#######################################
pause() {
    echo ''
    read -n 1 -rs -p 'Check for conflicts. Then, press any key to continue...'
    echo ''
}


#######################################
# Present directory menu.
# Globals:
#   dir_options
#######################################
dir_menu() {
    local directory

    PS3='Select a directory: '  # Set display prompt
    # Evaluate directory selection
    select directory in "${!dir_options[@]}"; do
        if ! [[ "${directory}" ]]; then
            echo -e '\nPlease enter a valid menu selection.\n' 1>&2

            dir_menu
            break
        elif [[ "${directory}" == 'Quit' ]]; then
            break
        else
            cd "${dir_options[${directory}]}" || exit 1
            
            work_menu
            break
        fi
    done
}


#######################################
# Present Work Menu.
# Globals:
#   fb
#   ft
#   fr
#   work_menu_post
#   work_menu_pre
#######################################
work_menu() {
    local choice curr_dir err_msg forked_repo menu_answer pass_or_fail \
        project_name upstream_repo

    # Get current directory
    curr_dir="Current directory: ${ft}$(current_dir="$(pwd)" &&
        echo "${current_dir##/*/}")${fr}\n"

    # Display menu options
    echo -e "${work_menu_pre}${curr_dir}${work_menu_post}"

    read -r menu_answer  # Save input to variable

    # Run commands that correspond to menu selection
    case "${menu_answer}" in
        'c')
            # Prompt for repository location
            echo''
            read -p 'Repository location? ' -r forked_repo
            read -p 'Upstream repository location (s to skip)? ' \
                -r upstream_repo
            # Clone repository
            git clone "${forked_repo}" &&
                project_name="${forked_repo##*/}" &&
                project_name="${project_name/%\.git/}"
            # Add upstream remote, if applicable
            if [[ "${upstream_repo}" != 's' ]]; then
                cd "${project_name}"  || exit 1
                git remote add upstream "${upstream_repo}"
                cd ..  # Move back to parent directory
            fi

            work_menu
            ;;
        'l')
            # Display long listing of projects
            find '.' -maxdepth 0 -exec ls --color=always -hl '{}' \; | less

            work_menu
            ;;
        's')
            while true; do
                # Prompt for directory name, save to variable
                echo''
                read -p 'Directory name? ' -r choice

                pass_or_fail="$(val_extant_dir "${choice}")"
                if [[ "${choice}" == 'q' ]]; then
                    exit 0
                elif [[ "${pass_or_fail}" -eq 0 ]]; then
                    break
                else
                    err_msg='\nInvalid directory. Try again or enter q '
                    err_msg+='to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            cd "${choice}" || exit 1  # Change to project directory

            git_menu
            ;;
        'd')
            dir_menu
            ;;
        'q')
            :
            ;;
        *)
            echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2

            work_menu
            ;;
    esac
}


#######################################
# Present Git Menu.
# Globals:
#   fb
#   ft
#   fr
#   git_menu_post
#   git_menu_pre
#   ter_emu
#######################################
git_menu() {
    local branch_name choice curr_dir err_msg menu_answer

    curr_dir="Current directory: ${ft}$(current_dir="$(pwd)" &&
        echo "${current_dir##/*/}")${fr}\n"

    # Display menu options
    echo -e "${git_menu_pre}${curr_dir}${git_menu_post}"

    read -r menu_answer  # Save input to variable

    # Run commands that correspond to menu selection
    case "${menu_answer}" in
        '1')
            git status | less

            git_menu
            ;;
        '2')
            while true; do
                # Prompt for diff choice
                echo''
                read -p 'Log with diff (y or n)? ' -r choice

                if [[ "${choice}" == 'y' || "${choice}" == 'n' ]]; then
                    break
                elif [[ "${choice}" == 'q' ]]; then
                    exit 0
                else
                    err_msg='\nInvalid input. Try again or enter q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            if [[ "${choice}" == 'y' ]]; then
                git log --color -p | less  # Present log with diff
            elif [[ "${choice}" == 'n' ]]; then
                # Present log without diff
                git log --all --color --decorate --graph | less
            fi

            git_menu
            ;;
        '3')
            # View differences between the working directory and staging index
            git diff --color | less

            git_menu
            ;;
        '4')
            while true; do
                # Prompt for view/edit choice
                echo''
                read -p 'View (v) or edit (e) config? ' -r choice

                if [[ "${choice}" == 'v' || "${choice}" == 'e' ]]; then
                    break
                elif [[ "${choice}" == 'q' ]]; then
                    exit 0
                else
                    err_msg='\nInvalid input. Try again or enter q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            if [[ "${choice}" == 'v' ]]; then
                less .git/config  # View config
            elif [[ "${choice}" == 'e' ]]; then
                "${EDITOR}" .git/config  # Edit config using system editor
            fi

            git_menu
            ;;
        '5')
            # View both local and remote branches
            git branch -a --color | less

            git_menu
            ;;
        '6')
            # Prompt for branch name, save to variable
            echo''
            read -p 'Branch name? ' -r choice
            git branch "${choice}"  # Create new branch

            git_menu
            ;;
        '7')
            # Prompt for branch name, save to variable
            echo''
            read -p 'Branch name? ' -r choice
            # Check out branch
            git checkout "${choice}" > '/dev/null' 2>&1 ||
                echo -e '\nBranch name not valid.' 1>&2

            git_menu
            ;;
        '8')
            git remote -v | less  # Display project remotes

            git_menu
            ;;
        '9')
            # Get current branch name
            branch_name="$(cat '.git/HEAD')"
            branch_name="${branch_name#ref: refs/heads/}"
            # Fetch and merge latest changes
            if git remote -v | grep -q 'upstream'; then
                git pull upstream "${branch_name}" | less
            else
                git pull origin "${branch_name}" | less
            fi

            git_menu
            ;;
        '10')
            # Get current branch name
            echo ''
            branch_name="$(cat '.git/HEAD')"
            branch_name="${branch_name#ref: refs/heads/}"
            # Fetch and merge latest changes
            if git remote -v | grep -q 'upstream'; then
                git pull upstream "${branch_name}" | less
            else
                git pull origin "${branch_name}" | less
            fi

            pause  # Pause script to check for conflicts

            while true; do
                # Prompt to continue or go back to menu
                echo''
                read -p 'Continue (c) or go back (b)? ' -r choice
                
                if [[ "${choice}" == 'c' || "${choice}" == 'b' ]]; then
                    break
                elif [[ "${choice}" == 'q' ]]; then
                    exit 0
                else
                    err_msg='\nInvalid input. Try again or enter q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done
            
            # Go back to menu if b is entered
            if [[ "${choice}" == 'b' ]]; then
                git_menu
            else
                # Add, commit, and push changes
                git add . && git commit -v && git push

                git_menu
            fi
            ;;
        't')
            # If terminal emulator is installed and using
            # a graphical environment
            if command -v "${ter_emu}" > '/dev/null' &&
                    [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                # Open project directory in a new terminal emulator window
                "${ter_emu}" "--working-directory=${PWD}" > '/dev/null' 2>&1
            else
                err_msg='\ngnome-terminal and a desktop environment must '
                err_msg+='be available to use this option.'
                echo -e "${err_msg}" 1>&2
            fi
            
            git_menu
            ;;
        'w')
            cd ..

            work_menu
            ;;
        'q')
            :
            ;;
        *)
            echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2

            git_menu
            ;;
    esac
}


#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    parse_args "${@}"

    run_init_cks

    dir_menu  # Display directory menu
}


###########
# Program #
###########
main "${@}"  # Start program
