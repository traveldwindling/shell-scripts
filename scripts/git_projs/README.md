# Git Projects

Manage various Git-based projects.

## Sample Output

```console
1) GitHub
2) GitLab
3) Salsa
4) Quit
Select a directory: 2

**************

Work Menu
Current directory: GitLab
~~~~~~~~~
c) Clone a Project
l) List Projects
s) Set Current Project
d) Directory Menu
q) Quit

Enter menu selection:
s

Directory name? inkscape

*************

Git Menu
Current directory: inkscape
~~~~~~~~
 1) Project Status
 2) Project Log
 3) Project diff
 4) Project config
 5) View All Project Branches
 6) Create a New Branch
 7) Switch to a New Branch
 8) View Project Remotes
 9) Pull Changes
10) Push Changes
 t) Open Current Directory in New Terminal
 w) Work Menu
 q) Quit

Enter menu selection:

```

## Required Packages/Scripts

### Debian

`# apt install git`

## Variables to Set

- `dir_options` - Associative array of directories that contain Git-based projects where the _key_ is the directory name to display in `dir_menu` and the _value_ is the absolute path to the directory.

    For example:

    ```bash
    dir_options=(
        ['GitHub']="${HOME}/github"
        ['GitLab']="${HOME}/gitlab"
        ['Salsa']="${HOME}/salsa"
        ['Quit']=''
    )
    ```

- `script_editor` - Program to use for editing files.

    By default, the script will respect your environment's `EDITOR` value. If this value is not configured, you can set `script_editor` to your editor of choice.