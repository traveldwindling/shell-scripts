# Create Virtual Environment

Create a new Python 3 virtual environment.

## Sample Output

```console
Enter virtual environment name: virtual_env_1
Collecting wheel
  Using cached wheel-0.43.0-py3-none-any.whl.metadata (2.2 kB)
Using cached wheel-0.43.0-py3-none-any.whl (65 kB)
Installing collected packages: wheel
Successfully installed wheel-0.43.0

Successfully created /home/mauian/venvs/virtual_env_1 virtual environment.

1) project_1
2) project_2
3) project_3
4) Quit
Select a project: 
```

## Required Packages/Scripts

### Debian

```console
# apt install \
    python3-pip \
    python3-venv
```

### Fedora

`# dnf install python3-pip`

## Variables to Set

- `projs_dir` (optional) - Directory of projects.
- `projs_dir_fltr` (optional) - Directories to filter from the projects directory.
- `ves_dir` - Directory to create virtual environment in.