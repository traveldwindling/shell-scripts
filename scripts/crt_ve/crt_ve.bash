#!/usr/bin/env bash

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)"
fr="$(tput sgr0)"

# Set virtual environments directory
ves_dir=""

# Set projects directory
projs_dir=""
# Define directories to filter out from the projects directory
projs_dir_fltr=()

name_regex='[^a-zA-Z0-9-]'  # Define name regular expression

# Create required modules array
req_mods=(
    'pip'
    'venv'
)


#############
# Functions #
#############
#######################################
# Display script usage.
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: . '${0}'

Purpose: Create a new Python virtual environment.

Requires:
    python3-pip
        pip is the Python package installer. This is the Python 3 version
        of the package.
    python3-venv (Debian)
        The venv module provides support for creating lightweight
        \"virtual environments\" with their own site directories,
        optionally isolated from system site directories.

Variables to set:
    projs_dir (optional)
        Directory of projects for use with virtual environments.
    projs_dir_fltr (optional)
        Directories to filter from the projects directory.
    ves_dir
        Directory to create virtual environment in.

Options:
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o h \
        --long help \
        -- "${@}")"

    # Check for invalid entries
    inv_ents="${?}" ; readonly inv_ents
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   fb
#   fr
#######################################
run_init_cks() {
    # Verify script was loaded via . command
    if ! [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
        echo -e "\nRerun script using the ${fb}.${fr} command." 1>&2 &&
            exit 1
    fi
}


#######################################
# Sanitize name.
# Globals:
#   name_regex
# Arguments:
#   File system object name to clean.
#######################################
clean_name() {
    local cleaned
    # Replace most non-alphanumeric characters with underscores
    cleaned="${1//${name_regex}/_}"
    # Squeeze underscores and lowercase name
    cleaned="$(echo -n "${cleaned}" | tr -s '_' | tr '[:upper:]' '[:lower:]')"
    echo "${cleaned}"
}


#######################################
# Filter an array.
# Arguments:
#   Directories to remove.
#   Array to filter.
#######################################
fltr_array() {
    # Create local named references
    local -n dirs_to_rm="${1}"
    local -n arr_to_fltr="${2}"

    local dir_to_rm idx

    for dir_to_rm in "${dirs_to_rm[@]}"; do
        for idx in "${!arr_to_fltr[@]}"; do
            # If directory to remove is in the array to filter, remove it
            if [[ "${arr_to_fltr[${idx}]}" == "${dir_to_rm}" ]]; then
                unset 'arr_to_fltr[idx]'
            fi
        done
    done
}


#######################################
# Display projects menu.
# Globals:
#   projs
#   projs_dir
#######################################
projs_menu() {
    local proj

    PS3='Select a project: '
    select proj in "${projs[@]}"; do
        if ! [[ "${proj}" ]]; then
            echo -e '\nPlease enter a valid menu selection.\n' 1>&2

            projs_menu
            break
        elif [[ "${proj}" == 'Quit' ]]; then
            break
        else
            cd "${projs_dir}/${proj}" || exit 1
            break
        fi
    done
}


#######################################
# Define starting point for execution of the program.
# Globals:
#   fb
#   fr
#   projs_dir
#   projs_dir_fltr
#   req_mods
#   req_vars
#   ves_dir
# Arguments:
#   All script arguments.
#######################################
main() {
    local err_msg mis_mods msg req_mod ve_dir ve_name

    parse_args "${@}"

    run_init_cks

    # Create missing modules array
    declare -a mis_mods
    # Verify that required modules are installed
    for req_mod in "${req_mods[@]}"; do
        if ! python3 -c "import ${req_mod}" > '/dev/null' 2>&1; then
            mis_mods+=("${req_mod}")
        fi
    done
    # Display error message if modules are missing
    if [[ "${mis_mods[*]}" ]]; then
        err_msg='\nInstall the following Python module(s) before '
        err_msg+='running the script again:'
        echo -e "${err_msg}\n${fb}${mis_mods[*]}${fr}" 1>&2
    fi

    # Verify that required variable is set
    if ! [[ "${ves_dir}" ]]; then
        # Prompt user to set required variable
        err_msg="\nSet ${fb}ves_dir${fr} before running the script again."
        echo -e "${err_msg}" 1>&2
    fi

    if [[ "${projs_dir}" ]]; then
        # Declare projects indexed array
        declare -a projs
        # Define projects array
        mapfile -t projs < \
            <(find "${projs_dir}" \
                -mindepth 1 \
                -maxdepth 1 \
                \( -type d -o -type l \) \
                -printf '%f\n'
            )
        # Filter projects directory array
        if [[ "${#projs_dir_fltr[@]}" -ne 0 ]]; then
            fltr_array projs_dir_fltr projs
        fi
        projs+=('Quit')  # Add quit option to projects array
    fi

    if ! [[ "${mis_mods[*]}" ]] && [[ "${ves_dir}" ]]; then
        # Capture virtual environment name
        echo ''
        read -p 'Enter virtual environment name: ' -r ve_name
        ve_name="$(clean_name "${ve_name}")"  # Sanitize input

        # Set virtual environment directory path
        ve_dir="${ves_dir}/${ve_name}"

        # If specified directory does not already exist
        if ! [[ -d "${ve_dir}" ]]; then
            # Create virtual environment, upgrade its core dependencies
            python3 -m venv --upgrade-deps "${ve_dir}"

            # Activate virtual environment
            # shellcheck disable=SC1091
            . "${ve_dir}/bin/activate"

            msg='\nSuccessfully created virtual environment:\n'
            msg+="${fb}'${ve_dir}'${fr}"
            echo -e "${msg}"

            if [[ "${projs_dir}" ]] && [[ "${#projs[@]}" -gt 1 ]]; then
                echo ''
                projs_menu  # Display projects menu
            fi
        else
            echo -e "\n${fb}'${ve_dir}'${fr} already exists."
        fi
    fi
}


###########
# Program #
###########
main "${@}"  # Start program
