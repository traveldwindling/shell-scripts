# Hardware Information

Display hardware diagnostic information.

## Sample Output

```console

*************************

Hardware Information
~~~~~~~~~~~~~~~~~~~~
 1) Interrupts
 2) I/O Ports
 3) Direct Memory Access
 4) USB
 5) Peripheral Component Interconnect
 6) CPU
 7) Memory
 8) Kernel Modules
 9) Block Devices
10) Network Devices
11) Available Power Sources
12) Power Source Object Details
 q) Quit

Enter menu selection:

```

## Variables to Set

- `scripts_dir` - Scripts directory.