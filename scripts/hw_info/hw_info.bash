#!/usr/bin/env bash

###########
# Aliases #
###########
shopt -s expand_aliases  # Enable alias expansion
# Set alias command options
alias less='less -Mr'
alias lsblk='lsblk -o NAME,PATH,MOUNTPOINT,TYPE,SIZE'

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
fr="$(tput sgr0)" ; readonly fr

# Define scripts directory
scripts_dir="" ; readonly scripts_dir

# Define absolute path regular expression
ab_path_regex='^\/.*[^'\''"]*$' ; readonly ab_path_regex

# Define menu characters and title
sep_char='*' ; readonly sep_char
menu_ti='Hardware Information' ; readonly menu_ti
und_char='~' ; readonly und_char

# Calculate menu length and generate decoration strings
mt_len="${#menu_ti}" ; readonly mt_len
sep_len="$(( mt_len + 5 ))" ; readonly sep_len
und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( mt_len ))"))"
readonly und_str
sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( sep_len ))"))"
readonly sep_str

# Define menu string
menu_str="
${sep_str}

${menu_ti}
${und_str}
 1) Interrupts
 2) I/O Ports
 3) Direct Memory Access
 4) USB
 5) Peripheral Component Interconnect
 6) CPU
 7) Memory
 8) Kernel Modules
 9) Block Devices
10) Network Devices
11) Available Power Sources
12) Power Source Object Details
 q) Quit

Enter menu selection:\
" ; readonly menu_str


#############
# Functions #
#############
#######################################
# Display script usage.
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: '${0}'

Purpose: Display hardware diagnostic information.

Variables to set:
    scripts_dir
        Scripts directory.

Options:
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


#######################################
# Verify path is absolute.
# Globals:
#   ab_path_regex
# Arguments:
#   Absolute path to validate.
#######################################
val_abs_path() {
    local to_test
    to_test="${1}"
    [[ "${to_test}" =~ ${ab_path_regex} ]] ; echo "${?}"
}


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o h \
        --long help \
        -- "${@}")"

    # Check for invalid entries
    inv_ents="${?}" ; readonly inv_ents
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   fb
#   fr
#   scripts_dir
#######################################
run_init_cks() {
    local err_msg

    # Verify that scripts_dir is set
    if ! [[ "${scripts_dir}" ]]; then
        # Prompt user to set required variable and exit
        err_msg="\nSet ${fb}scripts_dir${fr} before running the script again."
        echo -e "${err_msg}" 1>&2
        exit 1
    fi

    # Ensure that scripts_dir is part of PATH
    if ! [[ "${PATH}" =~ .*${scripts_dir}.* ]]; then
        err_msg="\nAdd ${fb}scripts_dir${fr} to the PATH variable and "
        err_msg+='run the script again.'
        echo -e "${err_msg}" 1>&2
        exit 1
    fi
}


#######################################
# Present menu.
# Globals:
#   fb
#   fr
#   menu_str
#######################################
menu() {
    local err_msg menu_answer obj_path pass_or_fail

    echo "${menu_str}"  # Display menu options

    read -r menu_answer  # Save input

    # Run commands that correspond to menu selection
    case "${menu_answer}" in
        '1')
            less '/proc/interrupts'

            menu
            ;;
        '2')
            less '/proc/ioports'

            menu
            ;;
        '3')
            less '/proc/dma'

            menu
            ;;
        '4')
            lsusb | sort | less

            menu
            ;;
        '5')
            lspci | less

            menu
            ;;
        '6')
            less '/proc/cpuinfo'

            menu
            ;;
        '7')
            less '/proc/meminfo'

            menu
            ;;
        '8')
            sort '/proc/modules' | less

            menu
            ;;
        '9')
            lsblk | less

            menu
            ;;
        '10')
            nmcli -p device show

            menu
            ;;
        '11')
            upower -e | less

            menu
            ;;
        '12')
            while true; do
                # Prompt for input, save to variable
                echo ''
                read -p 'Enter absolute object path: ' -r obj_path

                # Check for valid selection
                pass_or_fail="$(val_abs_path "${obj_path}")"
                if [[ "${obj_path}" == 'q' ]]; then
                    exit 0
                elif [[ "${pass_or_fail}" -eq 0 ]]; then
                    break
                else
                    err_msg='\nInvalid input. Try again or enter q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            upower -i "${obj_path}" | less

            menu
            ;;
        'q')
            :
            ;;
        *)
            echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2

            menu
            ;;
    esac
}


#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    parse_args "${@}"

    run_init_cks

    menu  # Call menu function
}


###########
# Program #
###########
main "${@}"  # Start program
