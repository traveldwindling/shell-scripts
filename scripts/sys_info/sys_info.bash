#!/usr/bin/env bash

###########
# Aliases #
###########
shopt -s expand_aliases  # Enable alias expansion
alias less='less -Mr'  # Set less options

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
fr="$(tput sgr0)" ; readonly fr

# Set main partition locations
deb_main='/' ; readonly deb_main
rh_main='/home' ; readonly rh_main
# Set network interface names
if grep -iq 'debian' '/etc/os-release'; then
    interface='' ; readonly interface
elif grep -iq 'fedora' '/etc/os-release'; then
    interface='' ; readonly interface
else
    err_msg='\nUnable to verify supported GNU/Linux '
    err_msg+='distribution.'
    echo -e "${err_msg}" 1>&2
    exit 1
fi

# Create required commands array
req_cmds=(
    'lsb_release'
    'lshw'
    'wget'
) ; readonly req_cmds


#############
# Functions #
#############
#######################################
# Display script usage.
# Globals:
#   fb
#   fr
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: '${0}'

Purpose: Display system overview information.

Requires:
    lsb_release
        print distribution-specific information
    lshw
        list hardware
    wget
        The non-interactive network downloader.

Variables to set:
    interface
        Name of network interface (e.g., ${fb}eth0${fr}).

Options:
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o h \
        --long help \
        -- "${@}")"

    # Check for invalid entries
    inv_ents="${?}" ; readonly inv_ents
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   fb
#   fr
#   interface
#   req_cmds
#######################################
run_init_cks() {
    local err_msg mis_cmds req_cmd

    # Create missing commands array
    declare -a mis_cmds
    # Verify that required commands are installed
    for req_cmd in "${req_cmds[@]}"; do
        if ! command -v "${req_cmd}" > '/dev/null' 2>&1; then
            mis_cmds+=("${req_cmd}")
        fi
    done
    # Exit script if commands are missing
    if [[ "${mis_cmds[*]}" ]]; then
        err_msg='\nInstall the following command(s) before '
        err_msg+='running the script again:'
        echo -e "${err_msg}\n${fb}${mis_cmds[*]}${fr}" 1>&2
        exit 1
    fi

    # Verify that required variable is set
    if ! [[ "${interface}" ]]; then
        # Prompt user to set required variable and exit
        err_msg="\nSet ${fb}interface${fr} before running the script again."
        echo -e "${err_msg}" 1>&2
        exit 1
    fi
}


#######################################
# Repeat given character for length of provided string.
# Arguments:
#   Character to repeat.
#   String whose length determines the number of character repeats.
#######################################
repeat(){
    local c
    for (( c=0; c < "${#2}"; c++ )); do
        echo -n "${1}"
    done
}


#######################################
# Define starting point for execution of the program.
# Globals:
#   deb_main
#   interface
#   rh_main
# Arguments:
#   All script arguments.
#######################################
main() {
    local avail cores cpu description err_msg gateway_ip graphics \
        main_partition mem_total mem_used num_of_pkgs percent_avail \
        percent_used public_ip py_ver tty_ up_time used

    parse_args "${@}"

    run_init_cks

    # Alter output on whether system is Debian or Fedora
    if lsb_release -i | grep -i -q 'debian'; then
        # Set partition and interface variables
        main_partition="${deb_main}"
        # Determine number of installed packages
        num_of_pkgs="$(dpkg -l | grep -c -E '^ii')"
    elif lsb_release -i | grep -i -q 'fedora'; then
        main_partition="${rh_main}"
        num_of_pkgs="$(rpm -q -a | wc -l)"
    else
        err_msg='\nUnable to verify supported GNU/Linux '
        err_msg+='distribution.'
        echo -e "${err_msg}" 1>&2
        exit 1
    fi

    {
        # Display system information
        echo 'System:'
        repeat '-' 'System:' && echo ''
        echo "User@Host: ${USER}@${HOSTNAME}"
        description="$(lsb_release -d)"
        echo "${description/Description:	/Distribution: }"
        echo -e "Kernel: $(uname -r)"
        up_time="$(uptime -p)"
        echo "Uptime: ${up_time/up /}"
        echo "Packages: ${num_of_pkgs}"
        echo "Bash: ${BASH_VERSION}"
        tty_="$(ps -p "${$}" | tail -n +2 | awk '{print $2}')"
        echo "TTY: ${tty_}"
        py_ver="$(python3 -V | awk '{print $2}')"
        echo "Python: ${py_ver}"
        cpu="$(lscpu | grep -E '^Model name:')"
        echo "CPU: ${cpu/Model name:                         /}"
        cores="$(nproc)"
        echo "Cores: ${cores}"
        graphics="$(lshw -class 'display' 2> '/dev/null' | grep 'product: ')"
        echo "Graphics: ${graphics/       product: /}"
        mem_used="$(free -h | grep -E '^Mem:' | awk '{print $3}')"
        mem_total="$(free -h | grep -E '^Mem:' | awk '{print $2}')"
        echo "Memory: ${mem_used} / ${mem_total}"
        echo -e '\n'

        # Determine used space values for main partition
        used="$(df -h "${main_partition}" | awk '{print $3}' | tail -n1)"
        percent_used="$(df -hT '/' | tail -n +2 | awk '{print $6}')"
        # Determine available space values for main partition
        avail="$(df -h "${main_partition}" | awk '{print $4}' | tail -n1)"
        percent_avail="$(( 100 - $(echo "${percent_used}" | tr -d '%') ))%"
        # Get gateway IP address
        gateway_ip="$(ip route show | grep 'default' | grep "${interface}" | 
            awk '{print $3}')"
        # Display storage information
        echo 'Storage:'
        repeat '-' 'Storage:' && echo ''
        echo "${main_partition} Used: ${used} (${percent_used})"
        echo "${main_partition} Available: ${avail} (${percent_avail})"
        echo -e "~ Used: $(du -hs "${HOME}" 2> '/dev/null' | 
            awk '{print $1}')\\n\\n"

        # Display top five home subdirectories taking up the most space
        echo 'Top Five in ~:'
        repeat '-' 'Top Five in ~:' && echo ''
        du -ha -d1 "${HOME}" 2> '/dev/null' | sort -hr | head -n 6 | tail -n +2
        echo -e '\n'

        # Display interface name, and system's local IP address and
        # gateway's IP address
        echo 'Networking:'
        repeat '-' 'Networking:' && echo ''
        echo "Interface: ${interface}"
        echo "Local IP Address: $(hostname -I | awk '{print $1}')"
        echo "Gateway IP Address: ${gateway_ip}"
        public_ip=" $(wget -q -O - 'ipinfo.io/ip')"
        echo "Public IP Address:${public_ip}"
    } | less
}


###########
# Program #
###########
main "${@}"  # Start program
