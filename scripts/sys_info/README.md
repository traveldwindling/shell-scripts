# System Information

Display system overview information.

## Sample Output

```console
System:
-------
User@Host: amnesia@nowhere
Distribution: Debian GNU/Linux 12 (bookworm)
Kernel: 6.1.0-17-amd64
Uptime: 3 minutes
Packages: 2541
Bash: 5.2.15(1)-release
TTY: pts/0
Python: 3.11.2
CPU: Intel Core Processor (Skylake, IBRS)
Cores: 2
Graphics: Virtio 1.0 GPU
Memory: 1.1Gi / 3.8Gi


Storage:
--------
/ Used: 13G (72%)
/ Available: 4.9G (28%)
~ Used: 113M


Top Five in ~:
--------------
85M     /home/amnesia/.cache
27M     /home/amnesia/.mozilla
672K    /home/amnesia/.local
520K    /home/amnesia/.config
12K     /home/amnesia/.gnupg


Networking:
-----------
Interface: enp1s0
Local IP Address: 10.0.2.15
Gateway IP Address: 10.0.2.2
Public IP Address: 144.74.108.246
```

## Required Packages/Scripts

### Debian

```console
# apt install \
    lshw \
    wget
```

### Fedora

```console
# dnf install \
    lshw \
    redhat-lsb-core
```

## Variables to Set

- `interface` - Name of network interface (e.g., `eth0`).