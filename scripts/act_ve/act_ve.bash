#!/usr/bin/env bash

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)"
fr="$(tput sgr0)"

# Set virtual environments directory
ves_dir=""
# Define directories to filter out from the virtual environments directory
ves_dir_fltr=()

# Set projects directory
projs_dir=""
# Define directories to filter out from the projects directory
projs_dir_fltr=()


#############
# Functions #
#############
#######################################
# Display script usage.
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: . '${0}'

Purpose: Activate a Python virtual environment.

Variables to set:
    projs_dir (optional)
        Directory of projects.
    projs_dir_fltr (optional)
        Directory names to filter from the projects directory.
    ves_dir
        Virtual environments directory.
    ves_dir_fltr (optional)
        Directory names to filter from the virtual environments directory.

Options:
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o h \
        --long help \
        -- "${@}")"

    # Check for invalid entries
    inv_ents="${?}" ; readonly inv_ents
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   fb
#   fr
#######################################
run_init_cks() {
    # Verify script was loaded via . command
    if ! [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
        echo -e "\nRerun script using the ${fb}.${fr} command." 1>&2 &&
            exit 1
    fi
}


#######################################
# Filter an array.
# Arguments:
#   Directories to remove.
#   Array to filter.
#######################################
fltr_array() {
    # Create local named references
    local -n dirs_to_rm="${1}"
    local -n arr_to_fltr="${2}"

    local dir_to_rm idx

    for dir_to_rm in "${dirs_to_rm[@]}"; do
        for idx in "${!arr_to_fltr[@]}"; do
            # If directory to remove is in the array to filter, remove it
            if [[ "${arr_to_fltr[${idx}]}" == "${dir_to_rm}" ]]; then
                unset 'arr_to_fltr[idx]'
            fi
        done
    done
}


#######################################
# Display virtual environment menu.
# Globals:
#   ves
#   ves_dir
#######################################
ve_menu() {
    PS3='Select a virtual environment: '
    select ve in "${ves[@]}"; do
        if ! [[ "${ve}" ]]; then
            echo -e '\nPlease enter a valid menu selection.\n' 1>&2

            ve_menu
            break
        elif [[ "${ve}" == 'Quit' ]]; then
            break
        else
            # shellcheck disable=SC1090
            . "${ves_dir}/${ve}/bin/activate"
            break
        fi
    done
}


#######################################
# Display projects menu.
# Globals:
#   projs
#   projs_dir
#######################################
projs_menu() {
    local proj

    PS3='Select a project: '
    select proj in "${projs[@]}"; do
        if ! [[ "${proj}" ]]; then
            echo -e '\nPlease enter a valid menu selection.\n' 1>&2

            projs_menu
            break
        elif [[ "${proj}" == 'Quit' ]]; then
            break
        else
            cd "${projs_dir}/${proj}" || exit 1
            break
        fi
    done
}


#######################################
# Define starting point for execution of the program.
# Globals:
#   fb
#   fr
#   projs_dir
#   projs_dir_fltr
#   req_vars
#   ves_dir
#   ves_dir_fltr
# Arguments:
#   All script arguments.
#######################################
main() {
    local err_msg

    parse_args "${@}"

    run_init_cks

    # Verify that required variable is set
    if ! [[ "${ves_dir}" ]]; then
        # Prompt user to set required variable
        err_msg="\nSet ${fb}ves_dir${fr} before running the script again."
        echo -e "${err_msg}" 1>&2
    else
        # Declare virtual environments indexed array
        declare -a ves
        # Define virtual environments array
        mapfile -t ves < \
            <(find "${ves_dir}" -mindepth 1 -maxdepth 1 -type d -printf '%f\n')
        # Filter virtual environments array
        if [[ "${#ves_dir_fltr[@]}" -ne 0 ]]; then
            fltr_array ves_dir_fltr ves
        fi
        ves+=('Quit')  # Add quit option to virtual environments array

        if [[ "${projs_dir}" ]]; then
            declare -a projs
            mapfile -t projs < \
                <(find "${projs_dir}" \
                    -mindepth 1 \
                    -maxdepth 1 \
                    \( -type d -o -type l \) \
                    -printf '%f\n'
                )
            # Filter projects directory array
            if [[ "${#projs_dir_fltr[@]}" -ne 0 ]]; then
                fltr_array projs_dir_fltr projs
            fi
            projs+=('Quit')
        fi

        ve_menu  # Display virtual environment menu

        if ! [[ "${ve}" == 'Quit' ]]; then
            if [[ "${projs_dir}" ]] && [[ "${#projs[@]}" -gt 1 ]]; then
                echo ''
                projs_menu  # Display projects menu
            fi
        fi
    fi
}


###########
# Program #
###########
main "${@}"  # Start program
