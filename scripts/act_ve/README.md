# Activate Virtual Environment

Activate a Python 3 virtual environment.

## Sample Output

```console
1) virtual_env_1
2) virtual_env_2
3) virtual_env_3
4) Quit
Select a virtual environment: 

1) project_1
2) project_2
3) project_3
4) Quit
Select a project: 
```

## Variables to Set

- `projs_dir` (optional) - Directory of projects.
- `projs_dir_fltr` (optional) - Directories to filter from the projects directory.
- `ves_dir` - The directory that contains the Python 3 virtual environments.
- `ves_dir_fltr` (optional) - Directories to filter from the virtual environments directory.