#!/usr/bin/env bash
# Purpose: Manage users and groups.

###########
# Aliases #
###########
shopt -s expand_aliases  # Enable alias expansion
alias less='less -Mr'  # Set less options

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
fr="$(tput sgr0)" ; readonly fr

ter_emu='gnome-terminal' ; readonly ter_emu  # Set script terminal emulator

# Define username regular expression
username_regex='^[a-z0-9_-]{3,15}$'

# Define menu characters and title
sep_char='*' ; readonly sep_char
menu_ti='Users and Groups' ; readonly menu_ti
und_char='~' ; readonly und_char

# Calculate menu length and generate decoration strings
mt_len="${#menu_ti}" ; readonly mt_len
sep_len="$(( mt_len + 5 ))" ; readonly sep_len
und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( mt_len ))"))"
readonly und_str
sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( sep_len ))"))"
readonly sep_str

# Define menu string
menu_str="
${sep_str}

${menu_ti}
${und_str}
 1) Display System Users
 2) Display System Groups
 3) Display Specific User
 4) Display Specific Group
 5) Add a New User
 6) Add a New Group
 7) Add an Existing User to an Existing Group
 8) Modify /etc/passwd or /etc/shadow
 9) Modify /etc/group or /etc/gshadow
10) Remove an Existing User From an Existing Group
 q) Quit

Enter menu selection:\
" ; readonly menu_str


#############
# Functions #
#############
# Validation Functions Start #
#######################################
# Verify string is valid username.
# Globals:
#   username_regex
# Arguments:
#   Username to validate.
#######################################
val_username() {
    local to_test
    to_test="${1}"
    [[ "${to_test}" =~ ${username_regex} ]] ; echo "${?}"
}


#######################################
# Verify extant user.
# Globals:
#   username
# Arguments:
#   Extant username to validate.
#######################################
is_extant_user() {
    local to_test
    to_test="${1}"
    getent passwd "${username}" > '/dev/null' 2>&1 ; echo "${?}"
}


#######################################
# Verify extant group.
# Globals:
#   username
# Arguments:
#   Extant group to validate.
#######################################
is_extant_group() {
    local to_test
    to_test="${1}"
    getent group "${group_name}" > '/dev/null' 2>&1 ; echo "${?}"
}
# Validation Functions End #


# Set Functions Start #
#######################################
# Set user.
# Arguments:
#   Prompt text.
#######################################
set_extant_user() {
    local err_msg pass_or_fail prompt

    prompt="${1}"
    while true; do
        read -p "${prompt}" -r username

        pass_or_fail="$(is_extant_user "${username}")"
        if [[ "${username}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail}" -eq 0 ]]; then
            break
        else
            err_msg='\nInvalid username. Try again or '
            err_msg+='enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done
}


#######################################
# Set group.
# Arguments:
#   Prompt text.
#######################################
set_extant_group() {
    local err_msg pass_or_fail prompt

    prompt="${1}"
    while true; do
        read -p "${prompt}" -r group_name

        pass_or_fail="$(is_extant_group "${group_name}")"
        if [[ "${group_name}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail}" -eq 0 ]]; then
            break
        else
            err_msg='\nInvalid group name. Try again or '
            err_msg+='enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done
}
# Set Functions End #


#######################################
# Present menu.
# Globals:
#   fb
#   fr
#   group_name
#   menu_str
#   ter_emu
#   username
#######################################
menu() {
    local choice err_msg full_name group_entry group_gid group_members \
        pass_or_fail prompt user_entry user_gecos user_gid user_home_dir \
        user_log_shell user_uid

    echo "${menu_str}"  # Display menu options

    read -r choice  # Save input

    # Run command(s) that correspond to menu choice
    case "${choice}" in
        '1')
            # Display list of usernames
            awk -F ':' '{print $1}' '/etc/passwd' | less
            menu
            ;;
        '2')
            # Display list of group names
            awk -F ':' '{print $1}' '/etc/group' | less
            menu
            ;;
        '3')
            echo ''
            prompt='Username? '
            set_extant_user "${prompt}"

            # Obtain user entry from /etc/passwd
            user_entry="$(getent passwd "${username}")"
            # User UID
            user_uid="$(awk -F ':' '{print $3}' <<< "${user_entry}")"
            # User GID
            user_gid="$(awk -F ':' '{print $4}' <<< "${user_entry}")"
            # User GECOS information
            user_gecos="$(awk -F ':' '{print $5}' <<< "${user_entry}")"
            # User home directory
            user_home_dir="$(awk -F ':' '{print $6}' <<< "${user_entry}")"
            # User login shell
            user_log_shell="$(awk -F ':' '{print $7}' <<< "${user_entry}")"

            {
                echo "Username: ${username}"
                echo "UID: ${user_uid}"
                echo "GID: ${user_gid}"
                echo "GECOS: ${user_gecos}"
                echo "Home Directory: ${user_home_dir}"
                echo "Login Shell: ${user_log_shell}"
            } | less  # Display user information

            menu
            ;;
        '4')
            echo ''
            prompt='Group name? '
            set_extant_group "${prompt}"

            # Obtain group entry from /etc/group
            group_entry="$(getent group "${group_name}")"
            # Group GID
            group_gid="$(awk -F ':' '{print $3}' <<< "${group_entry}")"
            # Group members
            group_members="$(awk -F ':' '{print $4}' <<< "${group_entry}")"

            {
                echo "Group name: ${group_name}"
                echo "GID: ${group_gid}"
                echo "Group members: ${group_members}"
            } | less  # Display group information

            menu
            ;;
        '5')
            while true; do
                echo''
                read -p 'Username? ' -r username

                pass_or_fail="$(val_username "${username}")"
                if [[ "${username}" == 'q' ]]; then
                    exit 0
                elif [[ "${pass_or_fail}" -eq 0 ]]; then
                    break
                else
                    err_msg='\nInvalid username. Try again or enter q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            # Alter output on whether system is Debian or Fedora
            if grep -iq 'debian' '/etc/os-release'; then
                sudo adduser "${username}"
            elif grep -iq 'fedora' '/etc/os-release'; then
                read -p 'Full Name? ' -r full_name
                full_name="${full_name//[^a-zA-Z]/}"  # Sanitize input

                sudo useradd \
                    -c "${full_name},,," \
                    -m \
                    -s '/bin/bash' \
                    "${username}"
                sudo passwd "${username}"
            else
                err_msg='\nUnable to verify supported GNU/Linux '
                err_msg+='distribution.'
                echo -e "${err_msg}" 1>&2
                exit 1
            fi

            menu
            ;;
        '6')
            while true; do
                echo''
                read -p 'New group name? ' -r group_name

                pass_or_fail="$(val_username "${group_name}")"
                if [[ "${group_name}" == 'q' ]]; then
                    exit 0
                elif [[ "${pass_or_fail}" -eq 0 ]]; then
                    break
                else
                    err_msg='\nInvalid group name. Try again or enter '
                    err_msg+='q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            # Alter output on whether system is Debian or Fedora
            if grep -iq 'debian' '/etc/os-release'; then
                sudo addgroup "${group_name}"
            elif grep -iq 'fedora' '/etc/os-release'; then
                sudo groupadd "${group_name}"
            else
                err_msg='\nUnable to verify supported GNU/Linux '
                err_msg+='distribution.'
                echo -e "${err_msg}" 1>&2
                exit 1
            fi

            menu
            ;;
        '7')
            echo ''
            prompt='Username? '
            set_extant_user "${prompt}"

            if [[ "${pass_or_fail}" -eq 0 ]]; then
                prompt='Group name? '
                set_extant_group "${prompt}"

                # Alter output on whether system is Debian or Fedora
                if grep -iq 'debian' '/etc/os-release'; then
                    sudo adduser "${username}" "${group_name}"
                elif grep -iq 'fedora' '/etc/os-release'; then
                    sudo gpasswd -a "${username}" "${group_name}"
                else
                    err_msg='\nUnable to verify supported GNU/Linux '
                    err_msg+='distribution.'
                    echo -e "${err_msg}" 1>&2
                    exit 1
                fi

                # Display restart message
                echo -e '\nRestart the system to see changes take effect.'
            fi

            menu
            ;;
        '8')
            while true; do
                echo''
                read -p 'Edit /etc/passwd (p) or /etc/shadow (s)? ' -r choice

                if [[ "${choice}" == 'p' || "${choice}" == 's' ]]; then
                    break
                elif [[ "${choice}" == 'q' ]]; then
                    exit 0
                else
                    err_msg='\nInvalid input. Try again or enter q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            if [[ "${choice}" == 'p' ]]; then
                # If terminal emulator is installed and using
                # a graphical environment
                if command -v "${ter_emu}" > '/dev/null' &&
                        [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                    "${ter_emu}" -- sudo vipw
                else
                    echo -e "\nExit script and enter ${fb}sudo vipw${fr}."
                fi
            elif [[ "${choice}" == 's' ]]; then
                if command -v "${ter_emu}" > '/dev/null' &&
                        [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                    "${ter_emu}" -- sudo vipw -s
                else
                    echo -e "\nExit script and enter ${fb}sudo vipw -s${fr}."
                fi
            fi

            menu
            ;;
        '9')
            while true; do
                echo''
                read -p 'Edit /etc/group (g) or /etc/gshadow (s)? ' -r choice
                
                if [[ "${choice}" == 'g' || "${choice}" == 's' ]]; then
                    break
                elif [[ "${choice}" == 'q' ]]; then
                    exit 0
                else
                    err_msg='\nInvalid input. Try again or enter q to quit.'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            if [[ "${choice}" == 'g' ]]; then
                # If terminal emulator is installed and using
                # a graphical environment
                if command -v "${ter_emu}" > '/dev/null' &&
                        [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                    "${ter_emu}" -- sudo vigr
                else
                    echo -e "\nExit script and enter ${fb}sudo vigr${fr}."
                fi
            elif [[ "${choice}" == 's' ]]; then
                if command -v "${ter_emu}" > '/dev/null' &&
                        [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                    "${ter_emu}" -- sudo vigr -s
                else
                    echo -e "\nExit script and enter ${fb}sudo vigr -s${fr}."
                fi
            fi

            menu
            ;;
        '10')
            echo ''
            prompt='Username? '
            set_extant_user "${prompt}"

            if [[ "${pass_or_fail}" -eq 0 ]]; then
                prompt='Group name? '
                set_extant_group "${prompt}"

                if grep -iq 'debian' '/etc/os-release'; then
                    sudo deluser "${username}" "${group_name}"
                elif grep -iq 'fedora' '/etc/os-release'; then
                    sudo gpasswd -d "${username}" "${group_name}"
                else
                    err_msg='\nUnable to verify supported GNU/Linux '
                    err_msg+='distribution.'
                    echo -e "${err_msg}" 1>&2
                    exit 1
                fi

                echo -e '\nRestart the system to see changes take effect.'
            fi

            menu
            ;;
        'q')
            :
            ;;
        *)
            echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2
            
            menu
            ;;
    esac
}


#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    menu  # Call menu function
}


###########
# Program #
###########
main "${@}"  # Start program
