# Users and Groups

Manage users and groups.

## Sample Output

```console

*********************

Users and Groups
~~~~~~~~~~~~~~~~
 1) Display system users
 2) Display system groups
 3) Display specific user
 4) Display specific group
 5) Add a new user
 6) Add a new group
 7) Add an existing user to an existing group
 8) Modify /etc/passwd or /etc/shadow
 9) Modify /etc/group or /etc/gshadow
10) Remove an existing user from an existing group
 q) Quit

Enter menu selection:

```