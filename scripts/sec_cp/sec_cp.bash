#!/usr/bin/env bash

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
ft="$(tput sitm)" ; readonly ft
fr="$(tput sgr0)" ; readonly fr

# Declare and define server associative array
declare -A server_options=(
    ['Change Mode']=''
    ['Quit']=''
) ; readonly server_options

# Define absolute path regular expression
ab_path_regex='^\/.*[^'\''"]*$' ; readonly ab_path_regex

# Create required commands array
req_cmds=(
    'scp'
    'ssh'
) ; readonly req_cmds

#############
# Functions #
#############
#######################################
# Display script usage.
# Globals:
#   fb
#   fr
#   ft
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: '${0}'

Purpose: Securely copy objects to/from servers.

Requires:
    scp
        OpenSSH secure file copy
    ssh
        OpenSSH remote login client

Variables to set:
    server_options
        The associative array that contains the default menu options
        (Change Mode, Quit) for the ${fb}select${fr} command. Key/value pairs
        for actual remote servers need to be added to this variable before
        script execution.
        
        For each pair, the ${ft}key${fr} is the remote host and the
        ${ft}value${fr} is the port number that ${fb}ssh${fr} should connect
        on. For example, ${fb}['darkseid@apokolips']='41970'${fr}.

Options:
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


# Validation Functions Start #
#######################################
# Verify path is absolute.
# Globals:
#   ab_path_regex
# Arguments:
#   Absolute path to validate.
#######################################
val_abs_path() {
    local to_test
    to_test="${1}"
    [[ "${to_test}" =~ ${ab_path_regex} ]] ; echo "${?}"
}


#######################################
# Verify object exists and is a directory.
# Arguments:
#   Extant directory to validate.
#######################################
val_extant_dir() {
    local to_test
    to_test="${1}"
    [[ -d "${to_test}" ]] ; echo "${?}"
}


#######################################
# Verify remote object exists and is a directory.
# Globals:
#   server
#   server_options
# Arguments:
#   Remote extant directory to validate.
#######################################
val_extant_dir_rem() {
    local to_test
    to_test="${1}"
    ssh -p "${server_options[${server}]}" "${server}" \
        [[ -d "${to_test}" ]] ; echo "${?}"
}


#######################################
# Verify object exists.
# Arguments:
#   Extant object to validate.
#######################################
val_extant_obj() {
    local to_test
    to_test="${1}"
    [[ -e "${to_test}" ]] ; echo "${?}"
}


#######################################
# Verify remote object exists.
# Globals:
#   server
#   server_options
# Arguments:
#   Remote extant object to validate.
#######################################
val_extant_obj_rem() {
    local to_test
    to_test="${1}"
    ssh -p "${server_options[${server}]}" "${server}" \
        [[ -e "${to_test}" ]] ; echo "${?}"
}
# Validation Functions End #


# Set Functions Start #
#######################################
# Set absolute object.
# Arguments:
#   Prompt text.
#######################################
set_abs_obj() {
    local err_msg pass_or_fail_1 pass_or_fail_2 prompt

    prompt="${1}"
    while true; do
        read -p "${prompt}" -r obj

        pass_or_fail_1="$(val_abs_path "${obj}")"
        pass_or_fail_2="$(val_extant_obj "${obj}")"
        if [[ "${obj}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail_1}" -eq 0 && "${pass_or_fail_2}" -eq 0 ]]; then
            obj="${obj%/}"  # Remove trailing slash if present
            break
        else
            err_msg='\nInvalid object path. Try again or '
            err_msg+='enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done
}


#######################################
# Set absolute remote object.
# Arguments:
#   Prompt text.
#######################################
set_abs_obj_rem() {
    local err_msg pass_or_fail_1 pass_or_fail_2 prompt

    prompt="${1}"
    while true; do
        read -p "${prompt}" -r obj

        pass_or_fail_1="$(val_abs_path "${obj}")"
        pass_or_fail_2="$(val_extant_obj_rem "${obj}")"
        if [[ "${obj}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail_1}" -eq 0 && "${pass_or_fail_2}" -eq 0 ]]; then
            obj="${obj%/}"  # Remove trailing slash if present
            break
        else
            err_msg='\nInvalid remote object path. Try again or '
            err_msg+='enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done
}


#######################################
# Set absolute directory.
# Arguments:
#   Prompt text.
#######################################
set_abs_dir() {
    local err_msg pass_or_fail_1 pass_or_fail_2 prompt

    prompt="${1}"
    while true; do
        read -p "${prompt}" -r obj

        pass_or_fail_1="$(val_abs_path "${obj}")"
        pass_or_fail_2="$(val_extant_dir "${obj}")"
        if [[ "${obj}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail_1}" -eq 0 && "${pass_or_fail_2}" -eq 0 ]]; then
            obj="${obj%/}"  # Remove trailing slash if present
            break
        else
            err_msg='\nInvalid directory path. Try again or '
            err_msg+='enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done
}


#######################################
# Set remote absolute directory.
# Arguments:
#   Prompt text.
#######################################
set_abs_dir_rem() {
    local err_msg pass_or_fail_1 pass_or_fail_2 prompt

    prompt="${1}"
    while true; do
        read -p "${prompt}" -r obj

        pass_or_fail_1="$(val_abs_path "${obj}")"
        pass_or_fail_2="$(val_extant_dir_rem "${obj}")"
        if [[ "${obj}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail_1}" -eq 0 && "${pass_or_fail_2}" -eq 0 ]]; then
            obj="${obj%/}"  # Remove trailing slash if present
            break
        else
            err_msg='\nInvalid remote directory path. Try again or '
            err_msg+='enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done
}
# Set Functions End #


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o h \
        --long help \
        -- "${@}")"

    # Check for invalid entries
    inv_ents="${?}" ; readonly inv_ents
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   fb
#   fr
#   req_cmds
#   server_options
#######################################
run_init_cks() {
    local err_msg mis_cmds req_cmd

    # Create missing commands array
    declare -a mis_cmds
    # Verify that required commands are installed
    for req_cmd in "${req_cmds[@]}"; do
        if ! command -v "${req_cmd}" > '/dev/null' 2>&1; then
            mis_cmds+=("${req_cmd}")
        fi
    done
    # Exit script if commands are missing
    if [[ "${mis_cmds[*]}" ]]; then
        err_msg='\nInstall the following command(s) before '
        err_msg+='running the script again:'
        echo -e "${err_msg}\n${fb}${mis_cmds[*]}${fr}" 1>&2
        exit 1
    fi

    # Ensure server_options array has been modified
    if [[ "${#server_options[*]}" -eq 2 ]]; then
        err_msg="\nAdd servers to ${fb}server_options${fr} "
        err_msg+='before running the script again.'
        echo -e "${err_msg}" 1>&2
        exit 1
    fi
}


#######################################
# Display mode menu.
#######################################
mode_menu() {
    local choice err_msg

    while true; do
        # Prompt for upload or download mode, save to variable
        echo''
        read -p 'Upload (u) or Download (d)? ' -r choice
        
        if [[ "${choice}" == 'u' || "${choice}" == 'd' ]]; then
            break
        elif [[ "${choice}" == 'q' ]]; then
            exit 0
        else
            err_msg='\nInvalid input. Try again or enter q to quit.'
            echo -e "${err_msg}" 1>&2
        fi
    done

    # Set value of output mode variable based on user input
    if [[ "${choice}" == 'u' ]]; then
        transfer_mode='Upload'
    elif [[ "${choice}" == 'd' ]]; then
        transfer_mode='Download'
    fi

    server_menu  # Call the server menu
}

#######################################
# Display server menu.
# Globals:
#   obj
#   server
#   server_options
#   transfer_mode
#######################################
server_menu() {
    local local_dir local_obj prompt remote_dir remote_obj

    if [[ "${transfer_mode}" ]]; then
        echo -e "\\nCurrent mode: ${transfer_mode}"  # Output transfer mode 
    fi

    PS3='Choose a server: '  # Set display prompt
    # Evaluate server selection
    select server in "${!server_options[@]}"; do
        if ! [[ "${server}" ]]; then
            echo -e '\nPlease enter a valid menu selection.' 1>&2

            server_menu
            break
        elif [[ "${server}" == 'Quit' ]]; then
            break
        elif [[ "${server}" == 'Change Mode' ]]; then
            mode_menu
            break
        else
            if [[ "${transfer_mode}" == 'Upload' ]]; then
                # Collect local object to upload input
                echo ''
                prompt='Local absolute object path to upload? '
                set_abs_obj "${prompt}"
                local_obj="${obj}"

                # Collect remote directory to upload to
                prompt='Remote absolute directory path to upload to? '
                set_abs_dir_rem "${prompt}"
                remote_dir="${obj}"

                # Upload object
                scp \
                    -P "${server_options[${server}]}" \
                    -r \
                    "${local_obj}" \
                    "${server}:${remote_dir}"

                server_menu
                break
            elif [[ "${transfer_mode}" == 'Download' ]]; then
                # Collect remote object to download input
                echo ''
                prompt='Remote absolute object path to download? '
                set_abs_obj_rem "${prompt}"
                remote_obj="${obj}"

                # Collect local directory to download to
                prompt='Local absolute directory path to download to? '
                set_abs_dir "${prompt}"
                local_dir="${obj}"

                # Download object
                scp \
                    -P "${server_options[${server}]}" \
                    -r \
                    "${server}:${remote_obj}" \
                    "${local_dir}"

                server_menu
                break
            fi
        fi
    done
}


#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    parse_args "${@}"

    run_init_cks

    mode_menu  # Call mode menu
}


###########
# Program #
###########
main "${@}"  # Start program
