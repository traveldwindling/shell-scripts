# Secure Copy

Securely copy file system objects to and from a server.

## Sample Output

```console

Upload (u) or Download (d)? d

Current mode: Download
1) diana_prince@96.180.246.25   3) Change Mode
2) Quit                         4) darkseid@apokolips
Choose a server: 4

Remote absolute object path to download? /home/darkseid/jl_plans.md

Local absolute directory path to download to? /home/orion/Downloads/
```

## Required Packages/Scripts

### Debian

`# apt install openssh-client`

## Variables to Set

- `server_options` - The associative array that contains the default menu options (`Change Mode`, `Quit`) for the `select` command. Key/value pairs for actual remote servers need to be added to this variable before script execution.

    For each pair, the _key_ is the remote host and the _value_ is the port number that `scp` should connect on. An example entry could be `['diana_prince@96.180.246.25']='22'` or `['darkseid@apokolips']='41970'`.

    The final `server_options` array would look like this using the prior examples:

        server_options=(
            ['diana_prince@96.180.246.25']='22'
            ['darkseid@apokolips']='41970'
            ['Change Mode']=''
            ['Quit']=''
        )