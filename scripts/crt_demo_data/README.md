# Create Demo Data

Create demo data directory.

## Sample Output

```console
$ pwd
/tmp/demo.MN0
$ tree --charset=ascii
.
|-- bar_1.txt
|-- bar_2.txt
|-- bar_3.txt
|-- foo_1
|   |-- bar_1.txt
|   |-- bar_2.txt
|   `-- bar_3.txt
|-- foo_2
|   |-- bar_1.txt
|   |-- bar_2.txt
|   `-- bar_3.txt
`-- foo_3
    |-- bar_1.txt
    |-- bar_2.txt
    `-- bar_3.txt

3 directories, 12 files
```