#!/usr/bin/env bash

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)"
fr="$(tput sgr0)"

# Define demo text
lorem_ipsum="\
Reiciendis et sit quidem et. Ut excepturi dolor eius sed provident consectetur aut. Est et molestiae ut vel tenetur est. Aut dolore voluptatum sequi culpa qui culpa.

Omnis non veritatis repellendus debitis. Itaque nobis amet quis aut eos id molestiae. Distinctio dolores minima vel ipsa consequuntur laborum. Sapiente placeat officia est. Facere ea ea cupiditate quo architecto magnam. Fugiat velit doloribus dolorem id.

Dolores aut et aut modi in earum. Magnam occaecati rerum omnis ea minima natus. Fugit nobis veritatis eos consequatur. Id blanditiis placeat veniam qui nobis temporibus consequatur aut.

Placeat perspiciatis exercitationem ipsum corporis doloremque cupiditate quisquam atque. Ab est sunt sequi possimus officiis quia. Omnis neque dignissimos sapiente molestiae.

Nobis illum blanditiis omnis sapiente corporis ea aut. Voluptas labore quos quia. Unde est autem illo debitis incidunt voluptates id accusamus. Corporis voluptates odio eveniet ipsum autem.\
"


#############
# Functions #
#############
#######################################
# Display script usage.
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: . '${0}'

Purpose: Create demo data for testing.

Options:
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o h \
        --long help \
        -- "${@}")"

    # Check for invalid entries
    inv_ents="${?}" ; readonly inv_ents
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   fb
#   fr
#######################################
run_init_cks() {
    # Verify script was loaded via . command
    if ! [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
        echo -e "\nRerun script using the ${fb}.${fr} command." 1>&2 && exit 1
    fi
}


#######################################
# Define starting point for execution of the program.
# Globals:
#   lorem_ipsum
# Arguments:
#   All script arguments.
#######################################
main() {
    local demo_dir inner_num num

    parse_args "${@}"

    run_init_cks

    # Create temporary directory for demo data
    demo_dir="$(mktemp -d '/tmp/demo.XXX')" && cd "${demo_dir}" || exit 1

    # Create demo data
    for num in {1..3}; do
        mkdir "foo_${num}"
        echo "${lorem_ipsum}" > "bar_${num}.txt"
        for inner_num in {1..3}; do
            echo "${lorem_ipsum}" > "foo_${num}/bar_${inner_num}.txt"
        done
    done

    cd "${demo_dir}" || exit 1  # Change to demo directory
}


###########
# Program #
###########
main "${@}"  # Start program
