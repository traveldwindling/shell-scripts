#!/usr/bin/env bash
# shellcheck disable=SC2086
# Purpose: Manage flatpak assets.

###########
# Aliases #
###########
shopt -s expand_aliases  # Enable alias expansion
alias less='less -Mr'  # Set less options

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
ft="$(tput sitm)" ; readonly ft
fr="$(tput sgr0)" ; readonly fr

ter_emu='gnome-terminal' ; readonly ter_emu  # Set script terminal emulator

# Set symbolic links directory
sym_links_dir="${HOME}/.local/share/flatpak/exports/bin"
readonly sym_links_dir
data_dir="${HOME}/.var/app" ; readonly data_dir  # Set data directory

# Define .flatpakrepo URL regular expression
url_regex='^(https:\/\/|http:\/\/).+(.flatpakrepo)$'

# Define menu characters and title
sep_char='*' ; readonly sep_char
menu_ti='Flatpak Management' ; readonly menu_ti
und_char='~' ; readonly und_char

# Calculate menu length and generate decoration strings
mt_len="${#menu_ti}" ; readonly mt_len
sep_len="$(( mt_len + 5 ))" ; readonly sep_len
und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( mt_len ))"))"
readonly und_str
sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( sep_len ))"))"
readonly sep_str

# Define menu string
menu_pre="
${sep_str}

${menu_ti}
" ; readonly menu_pre

menu_post="${und_str}
 1) Add a Flatpak Remote
 2) List Flatpak Remotes
 3) Remove a Flatpak Remote
 4) Display Flatpak Remote Runtimes and Apps
 5) Search a Flatpak Remote
 6) Install an App(s) From a Flatpak Remote
 7) Remove an App(s) and Associated Data
 8) List Installed Flatpak Apps
 9) Display Flatpak App Information
10) Display Flatpak App Permissions
11) Open Flatpak App Sym Link Directory
12) Open Flatpak App Data Directory
13) Open Flatpak Documentation Site
14) Set Flatpak Remote
 q) Quit

Enter menu selection:\
" ; readonly menu_post


#############
# Functions #
#############
# Validation Functions Start #
#######################################
# Verify flatpak remote is configured.
# Arguments:
#   Flatpak remote to validate.
#######################################
val_flatpak_rem() {
    local to_test
    to_test="${1}"
    flatpak remotes --user | grep -q "${to_test}" ; echo "${?}"
}


#######################################
# Verify flatpak app is installed.
# Arguments:
#   Flatpak app ID to validate.
#######################################
val_flatpak_app() {
    local to_test
    to_test="${1}"
    flatpak info "${to_test}" > '/dev/null' 2>&1 ; echo "${?}"
}


#######################################
# Verify string is valid .flatpakrepo URL.
# Arguments:
#   .flatpakrepo URL to validate.
#######################################
val_flatpakrepo_url() {
    local to_test
    to_test="${1}"
    [[ "${to_test}" =~ ${url_regex} ]] ; echo "${?}"
}
# Validation Functions End #


# Set Functions Start #
#######################################
# Set current flatpak remote.
# Arguments:
#   Prompt text.
#######################################
set_curr_fpr() {
    local err_msg pass_or_fail prompt

    prompt="${1}"
    while true; do
        read -p "${prompt}" -r curr_fpr

        pass_or_fail="$(val_flatpak_rem "${curr_fpr}")"
        if [[ "${curr_fpr}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail}" -eq 0 ]]; then
            break
        else
            err_msg='\nFlatpak remote is not configured. Try again or '
            err_msg+='enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done
}


#######################################
# Set flatpak app ID.
# Arguments:
#   Prompt text.
#######################################
set_flatpak_app() {
    local err_msg pass_or_fail prompt

    prompt="${1}"
    while true; do
        read -p "${prompt}" -r fp_app_id

        pass_or_fail="$(val_flatpak_app "${fp_app_id}")"
        if [[ "${fp_app_id}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail}" -eq 0 ]]; then
            break
        else
            err_msg='\nInvalid flatpak app ID. Try again or '
            err_msg+='enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done
}
# Set Functions End #


#######################################
# Run initialization checks.
# Globals:
#   fb
#   fr
#######################################
run_init_cks() {
    local err_msg

    # Verify that flatpak is installed
    if ! command -v 'flatpak' > '/dev/null' 2>&1; then
        # Prompt user to install flatpak and exit
        err_msg="\nInstall ${fb}flatpak${fr} before running "
        err_msg+='the script again.'
        echo -e "${err_msg}" 1>&2
        exit 1
    fi
}


#######################################
# Present menu.
# Globals:
#   curr_fpr
#   data_dir
#   fp_app_id
#   fb
#   fr
#   ft
#   menu_post
#   menu_pre
#   sym_links_dir
#   ter_emu
#######################################
menu() {
    local curr_fpr_str err_msg fp_app_ids fpr_name fpr_url menu_answer pass_or_fail prompt search_str

    if [[ "${curr_fpr}" ]]; then
        curr_fpr_str="Current Flatpak Remote: ${ft}${curr_fpr}${fr}\n"
    fi

    # Display menu options
    echo -e "${menu_pre}${curr_fpr_str}${menu_post}"

    read -r menu_answer  # Save input

    # Run command(s) that correspond to menu choice
    case "${menu_answer}" in
        '1')
            echo ''
            prompt='Enter local name to use for flatpak remote: '
            read -p "${prompt}" -r fpr_name

            while true; do
                read -p 'Enter .flatpakrepo URL: ' -r fpr_url

                pass_or_fail="$(val_flatpakrepo_url "${fpr_url}")"
                if [[ "${fpr_url}" == 'q' ]]; then
                    exit 0
                elif [[ "${pass_or_fail}" -eq 0 ]]; then
                    break
                else
                    err_msg='\nInvalid .flatpakrepo URL. Try again or '
                    err_msg+='enter q to quit.\n'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            flatpak remote-add \
                --user --if-not-exists "${fpr_name}" "${fpr_url}"

            menu
            ;;
        '2')
            flatpak remotes --user | less

            menu
            ;;
        '3')
            echo ''
            prompt='Enter local name of flatpak remote to remove: '
            read -p "${prompt}" -r fpr_name

            flatpak remote-delete --user "${fpr_name}"

            menu
            ;;
        '4')
            echo ''
            if ! [[ "${curr_fpr}" ]]; then
                prompt='Enter flatpak remote local name: '
                set_curr_fpr "${prompt}"
            fi

            flatpak remote-ls --user "${curr_fpr}" | sort | less

            menu
            ;;
        '5')
            echo ''
            if ! [[ "${curr_fpr}" ]]; then
                prompt='Enter flatpak remote local name: '
                set_curr_fpr "${prompt}"
            fi

            prompt='Enter string to search for on flatpak remote: '
            read -p "${prompt}" -r search_str

            flatpak remote-ls --user "${curr_fpr}" | sort | 
                grep --color=always -E -i ${search_str} | less

            menu
            ;;
        '6')
            echo ''
            if ! [[ "${curr_fpr}" ]]; then
                prompt='Enter flatpak remote local name: '
                set_curr_fpr "${prompt}"
            fi

            prompt='Enter flatpak app ID(s) to install: '
            read -p "${prompt}" -r fp_app_ids

            flatpak install --user "${curr_fpr}" ${fp_app_ids}

            menu
            ;;
        '7')
            echo ''
            prompt='Enter flatpak app ID(s) to remove: '
            read -p "${prompt}" -r fp_app_ids

            flatpak uninstall --user --delete-data ${fp_app_ids}

            menu
            ;;
        '8')
            flatpak list --user | sort | less

            menu
            ;;
        
        '9')
            echo ''
            prompt='Enter flatpak app ID: '
            set_flatpak_app "${prompt}"

            flatpak info "${fp_app_id}" | less

            menu
            ;;
        '10')
            echo ''
            prompt='Enter flatpak app ID: '
            set_flatpak_app "${prompt}"

            flatpak info -M "${fp_app_id}" | less

            menu
            ;;
        '11')
            # If terminal emulator is installed and using
            # a graphical environment
            if command -v "${ter_emu}" > '/dev/null' &&
                    [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                # Open directory in a new terminal emulator window
                "${ter_emu}" "--working-directory=${sym_links_dir}" \
                    > '/dev/null' 2>&1
            else
                err_msg='\ngnome-terminal and a desktop environment must '
                err_msg+='be available to use this option.'
                echo -e "${err_msg}" 1>&2
            fi            

            menu
            ;;
        '12')
            if command -v "${ter_emu}" > '/dev/null' &&
                    [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                "${ter_emu}" "--working-directory=${data_dir}" \
                    > '/dev/null' 2>&1
            else
                err_msg='\ngnome-terminal and a desktop environment must '
                err_msg+='be available to use this option.'
                echo -e "${err_msg}" 1>&2
            fi

            menu
            ;;
        '13')
            if [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                xdg-open 'https://docs.flatpak.org/en/latest/index.html' \
                    > '/dev/null' 2>&1
            else
                err_msg='\nA desktop environment must be available to '
                err_msg+='use this option.'
                echo -e "${err_msg}" 1>&2
            fi

            menu
            ;;
        '14')
            prompt='Enter flatpak remote local name: '
            set_curr_fpr "${prompt}"

            menu
            ;;
        'q')
            :
            ;;
        *)
            echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2

            menu
            ;;
    esac
}


#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    run_init_cks

    menu  # Call menu function
}


###########
# Program #
###########
main "${@}"  # Start program
