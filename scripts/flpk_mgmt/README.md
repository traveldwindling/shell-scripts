# Flatpak Management

Manage flatpak ecosystem.

_Note: Applicable `flatpak` commands use the `--user` option, which limits the scope of the commands to the current user's account._

## Sample Output

```console

***********************

Flatpak Management
~~~~~~~~~~~~~~~~~~
 1) Add a Flatpak Remote
 2) List Flatpak Remotes
 3) Remove a Flatpak Remote
 4) Display Flatpak Remote Runtimes and Apps
 5) Search a Flatpak Remote
 6) Install an App(s) From a Flatpak Remote
 7) Remove an App(s) and Associated Data
 8) List Installed Flatpak Apps
 9) Display Flatpak App Information
10) Display Flatpak App Permissions
11) Open Flatpak App Sym Link Directory
12) Open Flatpak App Data Directory
13) Open Flatpak Documentation Site
 q) Quit

Enter menu selection:

```

## Required Packages/Scripts

### Debian

`# apt install flatpak`