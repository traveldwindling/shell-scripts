#!/usr/bin/env bash
# Purpose: Manage common firewalld tasks.
# Requires: firewalld

###########
# Aliases #
###########
shopt -s expand_aliases  # Enable alias expansion
alias less='less -M'  # Set less options

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
ft="$(tput sitm)" ; readonly ft
fr="$(tput sgr0)" ; readonly fr

# Define menu characters and title
sep_char='*' ; readonly sep_char
menu_ti='firewalld Management' ; readonly menu_ti
und_char='~' ; readonly und_char

# Calculate menu length and generate decoration strings
mt_len="${#menu_ti}" ; readonly mt_len
sep_len="$(( mt_len + 5 ))" ; readonly sep_len
und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( mt_len ))"))"
readonly und_str
sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( sep_len ))"))"
readonly sep_str

# Define menu string
menu_pre="
${sep_str}

${menu_ti}
" ; readonly menu_pre

menu_post="\
${und_str}
 1) Display firewalld State
 2) Display Default Zone
 3) Display All Zones
 4) Display Active Zones
 5) Display Zone Configuration
 6) Display All Zones' Configurations
 7) Display Services
 8) Set Zone's Target
 9) Set Default Zone
10) Add/Remove Service To/From Zone
11) Add/Remove Source To/From Zone
12) Add/Remove Port To/From Zone
13) Set Configuration Mode
14) Set Current Zone
15) Open firewalld Documentation Site
 q) Quit

Enter menu selection:\
" ; readonly menu_post


#############
# Functions #
#############
# Set Functions Start #
#######################################
# Obtain and set configuration mode.
#######################################
set_cfg_mode() {
    local err_msg prompt

    while true; do
        echo ''
        prompt='Runtime (r) or Permanent (p) configuration? '
        read -p "${prompt}" -r cfg_mode

        if [[ "${cfg_mode}" == 'r' || "${cfg_mode}" == 'p' ]]; then
            break
        elif [[ "${cfg_mode}" == 'q' ]]; then
            exit 0
        else
            err_msg='\nInvalid input. Try again or enter q to quit.'
            echo -e "${err_msg}" 1>&2
        fi
    done
}


#######################################
# Obtain and set zone.
#######################################
set_zone() {
    # Prompt for input, save to variable
    read -p 'Zone name? ' -r z_name
}
# Set Functions End #


#######################################
# Run initialization checks.
# Globals:
#   fb
#   fr
#######################################
run_init_cks() {
    local err_msg

    # Verify that firewalld is installed
    if ! command -v '/usr/sbin/firewalld' > '/dev/null' 2>&1; then
        # Prompt user to install firewalld and exit
        err_msg="\nInstall ${fb}firewalld${fr} before running "
        err_msg+='the script again.'
        echo -e "${err_msg}" 1>&2
        exit 1
    fi
}


#######################################
# Determine add or remove action.
#######################################
add_or_remove() {
    local ar_type err_msg

    while true; do
        echo ''
        read -p 'Add (a) or Remove (r)? ' -r ar_type

        if [[ "${ar_type}" == 'a' || "${ar_type}" == 'r' ]]; then
            break
        elif [[ "${ar_type}" == 'q' ]]; then
            exit 0
        else
            err_msg='\nInvalid input. Try again or enter q to quit.'
            echo -e "${err_msg}" 1>&2
        fi
    done

    echo "${ar_type}"
}


#######################################
# Present menu.
# Globals:
#   cfg_mode
#   fb
#   fr
#   ft
#   menu_post
#   menu_pre
#   z_name
#######################################
menu() {
    # Define local variables
    local a_or_r cfg_mode_o curr_cfg_mode curr_zone err_msg menu_answer \
        prt_and_prot se_name src t_name

    # Build config mode output string
    if [[ "${cfg_mode}" ]]; then
        if [[ "${cfg_mode}" == 'r' ]]; then
            cfg_mode_o='Runtime'
        elif [[ "${cfg_mode}" == 'p' ]]; then
            cfg_mode_o='Permanent'
        fi

        curr_cfg_mode="Current Config Mode: ${ft}${cfg_mode_o}${fr}\n"
    fi

    # Build current zone string
    if [[ "${z_name}" ]]; then
        curr_zone="Current Zone: ${ft}${z_name}${fr}\n"
    fi

    # Display menu options
    echo -e "${menu_pre}${curr_cfg_mode}${curr_zone}${menu_post}"

    read -r menu_answer  # Save input

    # Run command(s) that correspond to menu choice
    case "${menu_answer}" in
        '1')
            sudo firewall-cmd --state | less

            menu
            ;;
        '2')
            firewall-cmd --get-default-zone | less

            menu
            ;;
        '3')
            if ! [[ "${cfg_mode}" ]]; then
                echo ''
                set_cfg_mode
            fi

            if [[ "${cfg_mode}" == 'r' ]]; then
                firewall-cmd --get-zones | tr ' ' '\n' | less
            elif [[ "${cfg_mode}" == 'p' ]]; then
                sudo firewall-cmd --permanent --get-zones | tr ' ' '\n' | less
            fi

            menu
            ;;
        '4')
            firewall-cmd --get-active-zones | less

            menu
            ;;
        '5')
            echo ''
            if ! [[ "${cfg_mode}" ]]; then
                set_cfg_mode
            fi

            if ! [[ "${z_name}" ]]; then
                set_zone
            fi

            if [[ "${cfg_mode}" == 'r' ]]; then
                sudo firewall-cmd --zone="${z_name}" --list-all | less
            elif [[ "${cfg_mode}" == 'p' ]]; then
                sudo firewall-cmd \
                    --permanent \
                    --zone="${z_name}" \
                    --list-all | less
            fi

            menu
            ;;
        '6')
            echo ''
            if ! [[ "${cfg_mode}" ]]; then
                set_cfg_mode
            fi

            if [[ "${cfg_mode}" == 'r' ]]; then
                sudo firewall-cmd --list-all-zones | less
            elif [[ "${cfg_mode}" == 'p' ]]; then
                sudo firewall-cmd --permanent --list-all-zones | less
            fi

            menu
            ;;
        '7')
            echo ''
            if ! [[ "${cfg_mode}" ]]; then
                set_cfg_mode
            fi

            if [[ "${cfg_mode}" == 'r' ]]; then
                firewall-cmd --get-services | tr ' ' '\n' | less
            elif [[ "${cfg_mode}" == 'p' ]]; then
                sudo firewall-cmd \
                --permanent \
                --get-services | tr ' ' '\n' | less
            fi

            menu
            ;;
        '8')
            echo ''
            if ! [[ "${z_name}" ]]; then
                set_zone
            fi

            read -p 'Target name? ' -r t_name

            {
                sudo firewall-cmd \
                    --permanent \
                    --zone="${z_name}" \
                    --set-target="${t_name}" &&
                    sudo firewall-cmd --reload
             } | less

            menu
            ;;
        '9')
            echo ''
            set_zone

            sudo firewall-cmd --set-default-zone="${z_name}" | less

            menu
            ;;
        '10')
            echo ''
            if ! [[ "${cfg_mode}" ]]; then
                set_cfg_mode
            fi

            a_or_r="$(add_or_remove)"

            if ! [[ "${z_name}" ]]; then
                set_zone
            fi

            read -p 'Service name? ' -r se_name

            if [[ "${cfg_mode}" == 'r' ]]; then
                if [[ "${a_or_r}" == 'a' ]]; then
                    sudo firewall-cmd \
                        --zone="${z_name}" \
                        --add-service="${se_name}" | less
                elif [[ "${a_or_r}" == 'r' ]]; then
                    sudo firewall-cmd \
                        --zone="${z_name}" \
                        --remove-service="${se_name}" | less
                fi
            elif [[ "${cfg_mode}" == 'p' ]]; then
                if [[ "${a_or_r}" == 'a' ]]; then
                    {
                        sudo firewall-cmd \
                        --permanent \
                        --zone="${z_name}" \
                        --add-service="${se_name}" &&
                        sudo firewall-cmd --reload
                    } | less
                elif [[ "${a_or_r}" == 'r' ]]; then
                    {
                        sudo firewall-cmd \
                        --permanent \
                        --zone="${z_name}" \
                        --remove-service="${se_name}" &&
                        sudo firewall-cmd --reload
                    } | less
                fi
            fi

            menu
            ;;
        '11')
            echo ''
            if ! [[ "${cfg_mode}" ]]; then
                set_cfg_mode
            fi

            a_or_r="$(add_or_remove)"

            if ! [[ "${z_name}" ]]; then
                set_zone
            fi

            read -p 'Source? ' -r src

            if [[ "${cfg_mode}" == 'r' ]]; then
                if [[ "${a_or_r}" == 'a' ]]; then
                    sudo firewall-cmd \
                        --zone="${z_name}" \
                        --add-source="${src}" | less
                elif [[ "${a_or_r}" == 'r' ]]; then
                    sudo firewall-cmd \
                        --zone="${z_name}" \
                        --remove-source="${src}" | less
                fi
            elif [[ "${cfg_mode}" == 'p' ]]; then
                if [[ "${a_or_r}" == 'a' ]]; then
                    {
                        sudo firewall-cmd \
                        --permanent \
                        --zone="${z_name}" \
                        --add-source="${src}" &&
                        sudo firewall-cmd --reload
                    } | less
                elif [[ "${a_or_r}" == 'r' ]]; then
                    {
                        sudo firewall-cmd \
                        --permanent \
                        --zone="${z_name}" \
                        --remove-source="${src}" &&
                        sudo firewall-cmd --reload
                    } | less
                fi
            fi

            menu
            ;;
        '12')
            echo ''
            if ! [[ "${cfg_mode}" ]]; then
                set_cfg_mode
            fi

            a_or_r="$(add_or_remove)"

            if ! [[ "${z_name}" ]]; then
                set_zone
            fi

            read -p 'Port/protocol? ' -r prt_and_prot

            if [[ "${cfg_mode}" == 'r' ]]; then
                if [[ "${a_or_r}" == 'a' ]]; then
                    sudo firewall-cmd \
                        --zone="${z_name}" \
                        --add-port="${prt_and_prot}" | less
                elif [[ "${a_or_r}" == 'r' ]]; then
                    sudo firewall-cmd \
                        --zone="${z_name}" \
                        --remove-port="${prt_and_prot}" | less
                fi
            elif [[ "${cfg_mode}" == 'p' ]]; then
                if [[ "${a_or_r}" == 'a' ]]; then
                    {
                        sudo firewall-cmd \
                        --permanent \
                        --zone="${z_name}" \
                        --add-port="${prt_and_prot}" &&
                        sudo firewall-cmd --reload
                    } | less
                elif [[ "${a_or_r}" == 'r' ]]; then
                    {
                        sudo firewall-cmd \
                        --permanent \
                        --zone="${z_name}" \
                        --remove-port="${prt_and_prot}" &&
                        sudo firewall-cmd --reload
                    } | less
                fi
            fi

            menu
            ;;
        '13')
            echo ''
            set_cfg_mode

            menu
            ;;
        '14')
            echo ''
            set_zone

            menu
            ;;
        '15')
            if [[ "${XDG_CURRENT_DESKTOP}" ]]; then
                xdg-open 'https://firewalld.org/documentation/' \
                    > '/dev/null' 2>&1
            else
                err_msg='\nA desktop environment must be available to '
                err_msg+='use this option.'
                echo -e "${err_msg}" 1>&2
            fi

            menu
            ;;
        'q')
            :
            ;;
        *)
            echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2

            menu
            ;;
    esac
}


#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    run_init_cks

    menu  # Call menu function
}


###########
# Program #
###########
main "${@}"  # Start program
