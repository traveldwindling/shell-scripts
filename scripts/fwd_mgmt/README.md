# firewalld Management

Manage common firewalld tasks.

_Note: Changes to the permanent configuration will automatically become part of the runtime configuration._

## Sample Output

```console

*************************

firewalld Management
~~~~~~~~~~~~~~~~~~~~
 1) Display firewalld State
 2) Display Default Zone
 3) Display All Zones
 4) Display Active Zones
 5) Display Zone Configuration
 6) Display All Zones' Configurations
 7) Display Services
 8) Set Zone's Target
 9) Set Default Zone
10) Add/Remove Service To/From Zone
11) Add/Remove Source To/From Zone
12) Add/Remove Port To/From Zone
13) Set Configuration Mode
14) Set Current Zone
15) Open firewalld Documentation Site
 q) Quit

Enter menu selection:

```

## Required Packages/Scripts

### Debian

`# apt install firewalld`