#!/usr/bin/env bash

###########
# Aliases #
###########
shopt -s expand_aliases  # Enable alias expansion
alias less='less -Mr'  # Set less options
alias grep='grep -E'  # Set grep options

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
ft="$(tput sitm)" ; readonly ft
fr="$(tput sgr0)" ; readonly fr

srch_dir=""  # Set default search directory
o_srch_type=''  # Set default output search type

# Define absolute path regular expression
ab_path_regex='^\/.*[^'\''"]*$' ; readonly ab_path_regex

# Define menu characters and title
sep_char='*' ; readonly sep_char
menu_ti='Find File Paths' ; readonly menu_ti
und_char='~' ; readonly und_char

# Calculate menu length and generate decoration strings
mt_len="${#menu_ti}" ; readonly mt_len
sep_len="$(( mt_len + 5 ))" ; readonly sep_len
und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( mt_len ))"))"
readonly und_str
sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( sep_len ))"))"
readonly sep_str

# Define menu string
menu_pre="
${sep_str}

${menu_ti}
" ; readonly menu_pre

menu_post="\
${und_str}
1) Set Search Type
2) Set Search Location
3) Set Search Term
e) Execute Search
q) Quit

Enter menu selection:\
" ; readonly menu_post

# Create required variables array
req_vars=(
    'srch_dir'
    'o_srch_type'
) ; readonly req_vars


#############
# Functions #
#############
#######################################
# Display script usage.
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: '${0}' [ex_option]

Purpose: Find file paths by filename text or file content.

         Text and PDF files are supported.

Requires:
    pdfgrep
        search PDF files for a regular expression

Variables to set:
    o_srch_type
        Default output search type ('Filename', 'File Contents').
    srch_dir
        Default directory to search.

Options:
    -c, --custom
        Custom initialization that enables non-default search type and
        directory values.
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


# Validation Functions Start #
#######################################
# Verify path is absolute.
# Globals:
#   ab_path_regex
# Arguments:
#   Absolute path to validate.
#######################################
val_abs_path() {
    local to_test
    to_test="${1}"
    [[ "${to_test}" =~ ${ab_path_regex} ]] ; echo "${?}"
}


#######################################
# Verify object exists and is a directory.
# Arguments:
#   Extant directory to validate.
#######################################
val_extant_dir() {
    local to_test
    to_test="${1}"
    [[ -d "${to_test}" ]] ; echo "${?}"
}


#######################################
# Verify selection.
# Arguments:
#   First selection option.
#   Second selection option.
#   Option selection to validate.
#######################################
val_sel() {
    local sel_1 sel_2 to_test
    sel_1="${1}"
    sel_2="${2}"
    to_test="${3}"
    [[ "${to_test}" == "${sel_1}" || "${to_test}" == "${sel_2}" ]]
    echo "${?}"
}


#######################################
# Verify term.
# Arguments:
#   Term to validate.
#######################################
val_term() {
    local to_test
    to_test="${1}"
    [[ -n "${to_test}" && "${to_test}" != 'q' ]] ; echo "${?}"
}
# Validation Functions End #


# Set Functions Start #
#######################################
# Set binary selection.
# Arguments:
#   Prompt text.
#######################################
set_sel() {
    local err_msg pass_or_fail prompt sel_1 sel_2

    prompt="${1}"
    # Extract selection options from prompt
    sel_1="${prompt#*(}"
    sel_1="${sel_1%%)*}"
    sel_2="${prompt#*(*(}"
    sel_2="${sel_2%%)*}"

    while true; do
        # Prompt for input, save to variable
        read -p "${prompt}" -r choice

        # Validate input
        pass_or_fail="$(val_sel "${sel_1}" "${sel_2}" "${choice}")"
        if [[ "${choice}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail}" -eq 0 ]]; then
            break
        else
            err_msg='\nInvalid selection. Try again or '
            err_msg+='enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done
}


#######################################
# Set output search type.
# Globals:
#   o_srch_type
#   srch_type
#######################################
set_o_srch_type() {
    if [[ "${srch_type}" == 'n' ]]; then
        o_srch_type='Filename'
    elif [[ "${srch_type}" == 'c' ]]; then
        o_srch_type='File Contents'
    fi
}


#######################################
# Set absolute directory.
# Arguments:
#   Prompt text.
#######################################
set_abs_dir() {
    local err_msg pass_or_fail_1 pass_or_fail_2 prompt

    prompt="${1}"
    while true; do
        read -p "${prompt}" -r entd_dir

        pass_or_fail_1="$(val_abs_path "${entd_dir}")"
        pass_or_fail_2="$(val_extant_dir "${entd_dir}")"
        if [[ "${entd_dir}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail_1}" -eq 0 && "${pass_or_fail_2}" -eq 0 ]]; then
            entd_dir="${entd_dir%/}"  # Remove trailing slash if present
            break
        else
            err_msg='\nInvalid directory path. Try again or '
            err_msg+='enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done
}


#######################################
# Set term.
# Arguments:
#   Prompt text.
#######################################
set_term() {
    local err_msg pass_or_fail prompt

    prompt="${1}"
    while true; do
        read -p "${prompt}" -r term

        # Validate input
        pass_or_fail="$(val_term "${term}")"
        if [[ "${term}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail}" -eq 0 ]]; then
            break
        else
            err_msg='\nInvalid input. Try again or enter q to quit.'
            echo -e "${err_msg}" 1>&2
        fi
    done
}
# Set Functions End #


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o hc \
        --long help,custom \
        -- "${@}")"

    inv_ents="${?}"  # Check for invalid entries
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '-c' | '--custom')
                c_on='true'
                shift
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   fb
#   fr
#   req_vars
#######################################
run_init_cks() {
    local err_msg req_var unset_vars

    # Verify that command is installed
    if ! command -v 'pdfgrep' > '/dev/null' 2>&1; then
        # Prompt user to install command and exit
        err_msg="\nInstall ${fb}pdfgrep${fr} before running "
        err_msg+='the script again.'
        echo -e "${err_msg}" 1>&2
        exit 1
    fi

    # Create unset variables array
    declare -a unset_vars
    # Verify that required variables are set
    for req_var in "${req_vars[@]}"; do
        if ! [[ "${!req_var}" ]]; then
            unset_vars+=("${req_var}")
        fi
    done
    # Exit script if variables are not set
    if [[ "${unset_vars[*]}" ]]; then
        err_msg='\nSet the following variable(s) before '
        err_msg+='running the script again:'
        echo -e "${err_msg}\n${fb}${unset_vars[*]}${fr}" 1>&2
        exit 1
    fi
}


#######################################
# Execute search.
# Globals:
#   o_srch_type
#   srch_dir
#   srch_term
#######################################
exec_srch() {
    local find_o grep_o pdfgrep_o

    # If search type is filename
    if [[ "${o_srch_type}" == 'Filename' ]]; then
        find_o="$(find \
                  "${srch_dir}" \
                  -type d \( -path '/proc/' -o -path '/run/' \) -prune -o \
                  -iname "*${srch_term}*" -print 2>&1 |
                   grep --color=always -i "${srch_term}")"
        echo "${find_o}" | less
    # If search type is file content
    elif [[ "${o_srch_type}" == 'File Contents' ]]; then
        grep_o="$(cd "${srch_dir}" &&
                  grep -iIlr "${srch_term}" 2>&1 |
                  grep -v 'Permission denied')"
        pdfgrep_o="$(pdfgrep -ilr \
                     "${srch_term}" \
                     "${srch_dir}" 2>&1 | grep -v 'Permission denied')"

        # Display concatenated results
        echo -e "${grep_o}\n${pdfgrep_o}" | less
    fi
}


#######################################
# Present menu.
# Globals:
#   choice
#   entd_dir
#   fb
#   fr
#   ft
#   menu_post
#   menu_pre
#   o_srch_type
#   srch_dir
#   srch_term
#   term
#######################################
menu() {
    local curr_srch_dir curr_srch_term curr_srch_type menu_answer menu_o prompt

    # Build current search type string
    if [[ "${o_srch_type}" ]]; then
        curr_srch_type="Search Type: ${ft}${o_srch_type}${fr}\n"
    fi

    # Build current search location string
    if [[ "${srch_dir}" ]]; then
        curr_srch_dir="Search Location: ${ft}${srch_dir}${fr}\n"
    fi

    # Build current search term string
    if [[ "${srch_term}" ]]; then
        curr_srch_term="Search Term: ${ft}${srch_term}${fr}\n"
    fi

    # Display menu options
    menu_o="${menu_pre}${curr_srch_type}${curr_srch_dir}"
    menu_o+="${curr_srch_term}${menu_post}"
    echo -e "${menu_o}"

    read -r menu_answer  # Save input

    # Run commands that correspond to menu selection
    case "${menu_answer}" in
        '1')
            # Obtain search type and save to variable
            echo ''
            prompt='Search filenames (n) or file contents (c)? '
            set_sel "${prompt}"
            srch_type="${choice}"
            set_o_srch_type

            menu
            ;;
        '2')
            echo ''
            prompt='Enter absolute path of search directory: '
            set_abs_dir "${prompt}"
            srch_dir="${entd_dir}"

            menu
            ;;
        '3')
            echo ''
            prompt='Enter search term: '
            set_term "${prompt}"
            srch_term="${term}"

            exec_srch

            menu
            ;;
        'e')
            exec_srch

            menu
            ;;
        'q')
            :
            ;;
        *)
            echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2
            
            menu
            ;;
    esac
}


#######################################
# Define starting point for execution of the program.
# Globals:
#   c_on
#   choice
#   entd_dir
#   fb
#   fr
#   term
# Arguments:
#   All script arguments.
#######################################
main() {
    local prompt

    parse_args "${@}"

    run_init_cks

    if [[ "${c_on}" == 'true' ]]; then
        echo -e "\n${fb}Initialization Start${fr}"
        # Set search type
        prompt='Search filenames (n) or file contents (c)? '
        set_sel "${prompt}"
        srch_type="${choice}"
        set_o_srch_type

        # Set search directory
        echo ''
        prompt='Enter absolute path of search directory: '
        set_abs_dir "${prompt}"
        srch_dir="${entd_dir}"

        # Set search term
        echo ''
        prompt='Enter search term: '
        set_term "${prompt}"
        srch_term="${term}"
        exec_srch
        echo -e "${fb}Initialization End${fr}"
    else
        echo -e "\n${fb}Initialization Start${fr}"
        prompt='Enter search term: '
        set_term "${prompt}"
        srch_term="${term}"
        exec_srch
        echo -e "${fb}Initialization End${fr}"
    fi

    menu  # Call menu function
}


###########
# Program #
###########
main "${@}"  # Start program
