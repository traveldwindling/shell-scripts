# Find File Paths

Find file paths by filename text or file content.

## Sample Output

```console

Initialization Start
Enter search term: python
Initialization End

********************

Find File Paths
Search Type: Filename
Search Location: /home/monty/programming
Search Term: python
~~~~~~~~~~~~~~~
1) Set Search Type
2) Set Search Location
3) Set Search Term
e) Execute Search
q) Quit

Enter menu selection:

```

## Required Packages/Scripts

### Debian

`# apt install pdfgrep`

### Fedora

`# dnf install pdfgrep`

## Variables to Set

- `o_srch_type` - Default output search type ('Filename', 'File Contents').
- `srch_dir` - Default directory to search.