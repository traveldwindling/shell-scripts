#!/usr/bin/env bash
# Purpose: Display established connection information.

###########
# Program #
###########
# Declare arrays for IP address and process information
declare -a l_ips_array proc_info_a r_ips_array

# Extract local IP addresses for non-listening connections
l_ips="$(ss -ptu | awk '{print $5}' | tail -n +2 | sed -e 's/:.*//')"

# Extract peer (remote) IP addresses for non-listening connections
r_ips="$(ss -ptu | awk '{print $6}' | tail -n +2 | sed -e 's/:.*//')"

# Extract process information for non-listening connections
p_info="$(ss -ptu | awk '{print $7}' | tail -n +2)"

# Convert strings into indexed arrays
while read -r line; do
    l_ips_array+=("${line}")
done <<< "${l_ips}"

while read -r line; do
    r_ips_array+=("${line}")
done <<< "${r_ips}"

while read -r line; do
    proc_info_a+=("${line}")
done <<< "${p_info}"

# Loop through arrays and output connection information
for (( i=0; i < "${#r_ips_array[@]}"; i++ )); do
    echo "Connection $(( i + 1 ))"
    echo "Local I.P.: ${l_ips_array[${i}]}"
    echo "Remote I.P.: ${r_ips_array[${i}]}"
    # Obtain hostname associated with remote IP address
    empty_or_not="$(dig +short -x "${r_ips_array[${i}]}")"
    remote_hostname='Remote Hostname: '
    remote_hostname+="${empty_or_not:-No host name information available.}"
    echo "${remote_hostname}"
    proc_info='Process: '
    proc_info+="${proc_info_a[${i}]:-No process information available.}"
    proc_info+='\n'
    echo -e "${proc_info}"
done | less -M
