# Established Connections

Display established connection information.

## Sample Output

```console
Connection 1
Local I.P.: 10.0.2.15%enp1s0
Remote I.P.: 10.0.2.2
Remote Hostname: No host name information available.
Process: No process information available.

Connection 2
Local I.P.: 10.0.2.15
Remote I.P.: 142.250.191.35
Remote Hostname: nuq04s42-in-f3.1e100.net.
Process: users:(("firefox-esr",pid=2641,fd=108))

Connection 3
Local I.P.: 10.0.2.15
Remote I.P.: 13.35.122.101
Remote Hostname: server-13-35-122-101.sfo5.r.cloudfront.net.
Process: users:(("firefox-esr",pid=2641,fd=121))

Connection 4
Local I.P.: 10.0.2.15
Remote I.P.: 99.84.224.96
Remote Hostname: server-99-84-224-96.sfo5.r.cloudfront.net.
Process: users:(("firefox-esr",pid=2641,fd=113))

Connection 5
Local I.P.: 10.0.2.15
Remote I.P.: 198.35.26.96
Remote Hostname: text-lb.ulsfo.wikimedia.org.
Process: users:(("firefox-esr",pid=2641,fd=122))
```