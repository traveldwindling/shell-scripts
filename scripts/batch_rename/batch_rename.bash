#!/usr/bin/env bash
# Purpose: Change file extensions, or prepend/append text
# to file system object names.

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
ft="$(tput sitm)" ; readonly ft
fr="$(tput sgr0)" ; readonly fr

# Define absolute path regular expression
ab_path_regex='^\/.*[^'\''"]*$' ; readonly ab_path_regex
# Define file extention regular expression
file_ext_regex='[a-z0-9]+' ; readonly file_ext_regex

# Define menu characters and title
sep_char='*' ; readonly sep_char
menu_ti='Batch Object Renaming' ; readonly menu_ti
und_char='~' ; readonly und_char

# Calculate menu length and generate decoration strings
mt_len="${#menu_ti}" ; readonly mt_len
sep_len="$(( mt_len + 5 ))" ; readonly sep_len
und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( mt_len ))"))"
readonly und_str
sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( sep_len ))"))"
readonly sep_str

# Define menu string
menu_pre="
${sep_str}

${menu_ti}
" ; readonly menu_pre

menu_post="\
${und_str}
c) Change File Extensions
p) Prepend Text to Object Names
a) Append Text to Object Names
s) Set Working Directory
q) Quit

Enter menu selection:\
" ; readonly menu_post


#############
# Functions #
#############
# Validation Functions Start #
#######################################
# Verify path is absolute.
# Globals:
#   ab_path_regex
# Arguments:
#   Absolute path to validate.
#######################################
val_abs_path() {
    local to_test
    to_test="${1}"
    [[ "${to_test}" =~ ${ab_path_regex} ]] ; echo "${?}"
}


#######################################
# Verify object exists and is a directory.
# Arguments:
#   Extant directory to validate.
#######################################
val_extant_dir() {
    local to_test
    to_test="${1}"
    [[ -d "${to_test}" ]] ; echo "${?}"
}


#######################################
# Verify string is valid file extension.
# Globals:
#   file_ext_regex
# Arguments:
#   File extension to validate.
#######################################
val_file_ext() {
    local to_test
    to_test="${1}"
    [[ "${to_test}" =~ ${file_ext_regex} ]] ; echo "${?}"
}
# Validation Functions End #


#######################################
# Set absolute directory.
# Arguments:
#   Prompt text.
#######################################
set_abs_dir() {
    local err_msg pass_or_fail_1 pass_or_fail_2 prompt

    prompt="${1}"
    while true; do
        read -p "${prompt}" -r entd_dir

        pass_or_fail_1="$(val_abs_path "${entd_dir}")"
        pass_or_fail_2="$(val_extant_dir "${entd_dir}")"
        if [[ "${entd_dir}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail_1}" -eq 0 && "${pass_or_fail_2}" -eq 0 ]]; then
            entd_dir="${entd_dir%/}"  # Remove trailing slash if present
            break
        else
            err_msg='\nInvalid directory path. Try again or '
            err_msg+='enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done
}


#######################################
# Change file extensions.
#######################################
change_ext() {
    local b err_msg f new_ext old_ext pass_or_fail

    while true; do
        # Obtain old extension
        echo''
        read -p 'Enter old extension (e.g., txt): ' -r old_ext

        pass_or_fail="$(val_file_ext "${old_ext}")"
        if [[ "${old_ext}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail}" -eq 0 ]]; then
            break
        else
            err_msg='\nInvalid file extension. Try again or enter q to quit.'
            echo -e "${err_msg}" 1>&2
        fi
    done

    # Obtain new extension
    while true; do
        read -p 'Enter new extension (e.g., md): ' -r new_ext
        
        pass_or_fail="$(val_file_ext "${new_ext}")"
        if [[ "${new_ext}" == 'q' ]]; then
            exit 0
        elif [[ "${pass_or_fail}" -eq 0 ]]; then
            break
        else
            err_msg='\nInvalid file extension. Try again or enter q to quit.\n'
            echo -e "${err_msg}" 1>&2
        fi
    done

    # Rename each file in current directory with old extension
    # with new extension
    for f in ./*."${old_ext}"; do
        if [[ -f "${f}" ]]; then
            b=$(basename "${f}")
            mv "${f}" "${b%.*}.${new_ext}"
        fi
    done
}


#######################################
# Prepend text to objects.
#######################################
prepend_text() {
    local b obj text_to_prepend text_to_replace

    # Obtain old text to replace and new text to prepend
    echo ''
    read -p 'Enter existing text to replace: ' -r text_to_replace
    read -p 'Enter new text to prepend: ' -r text_to_prepend

    # Loop through all objects in the current directory
    for obj in ./*; do
        # Save object basename to a variable
        b=$(basename "${obj}")

        # If text to replace is empty, rename file by prepending text
        # to prepend
        # Else, rename file by replacing text to replace with text to prepend
        if [[ -z "${text_to_replace}" ]]; then
            mv "${obj}" "${text_to_prepend}${b}"
        else
            b="${b/#${text_to_replace}/${text_to_prepend}}"
            mv "${obj}" "${b}"
        fi
    done
}


#######################################
# Append text to objects.
#######################################
append_text() {
    local b b_no_ext ext obj text_to_append text_to_replace

    # Obtain old text to replace and new text to append
    echo ''
    read -p 'Enter existing text to replace: ' -r text_to_replace
    read -p 'Enter new text to append: ' -r text_to_append

    for obj in ./*; do
        # Save object basename, object basename without an extension,
        # and extension
        b=$(basename "${obj}")
        b_no_ext="${b%.*}"
        ext="${b##*.}"

        # If object name has an extension
        if [[ "${obj}" =~ ^[.]/.*[.] ]]; then
            # If text to replace is empty
            if [[ -z "${text_to_replace}" ]]; then
                mv "${obj}" "${b_no_ext}${text_to_append}.${ext}"
            else
                b="${b/${text_to_replace}/${text_to_append}}"
                mv "${obj}" "${b}"
            fi
        else
            if [[ -z "${text_to_replace}" ]]; then
                mv "${obj}" "${b}${text_to_append}"
            else
                b="${b/$text_to_replace/$text_to_append}"
                mv "${obj}" "${b}"
            fi
        fi
    done
}


#######################################
# Present menu.
# Globals:
#   entd_dir
#   fr
#   ft
#   menu_post
#   menu_pre
#######################################
menu() {
    local menu_answer prompt working_dir

    if [[ "${entd_dir}" ]]; then
        working_dir="Working directory: ${ft}$(basename "${entd_dir}")${fr}\n"
    fi

    echo -e "${menu_pre}${working_dir}${menu_post}"  # Display menu options

    read -r menu_answer  # Save input

    # Set prompt for set_abs_dir function calls
    prompt='Enter absolute path of working directory: '

    # Run commands that correspond to menu selection
    case "${menu_answer}" in
        'c')
            if ! [[ "${entd_dir}" ]]; then
                echo ''
                set_abs_dir "${prompt}"

                cd "${entd_dir}" || exit 1
            fi

            change_ext

            menu
            ;;
        'p')
            if ! [[ "${entd_dir}" ]]; then
                echo ''
                set_abs_dir "${prompt}"

                cd "${entd_dir}" || exit 1
            fi

            prepend_text

            menu
            ;;
        'a')
            if ! [[ "${entd_dir}" ]]; then
                echo ''
                set_abs_dir "${prompt}"

                cd "${entd_dir}" || exit 1
            fi

            append_text

            menu
            ;;
        's')
            echo ''
            set_abs_dir "${prompt}"

            cd "${entd_dir}" || exit 1

            menu
            ;;
        'q')
            :
            ;;
        *)
            echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2
            
            menu
            ;;
    esac
}


#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    menu  # Call menu function
}


###########
# Program #
###########
main "${@}"  # Start program
