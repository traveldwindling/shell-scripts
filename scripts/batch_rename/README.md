# Batch Object Renaming

Change file extensions, or prepend/append text to file system object names.

## Sample Output

```console

**************************

Batch Object Renaming
~~~~~~~~~~~~~~~~~~~~~
c) Change file extensions
p) Prepend text to object names
a) Append text to object names
s) Set working directory
q) Quit

Enter menu selection:

```