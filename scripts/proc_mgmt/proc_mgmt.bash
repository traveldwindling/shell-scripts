#!/usr/bin/env bash
# Purpose: Display and manage processes.

###########
# Aliases #
###########
shopt -s expand_aliases  # Enable alias expansion
alias less='less -Mr'  # Set less options

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
fr="$(tput sgr0)" ; readonly fr

# Define menu characters and title
sep_char='*' ; readonly sep_char
menu_ti='Process Management' ; readonly menu_ti
und_char='~' ; readonly und_char

# Calculate menu length and generate decoration strings
mt_len="${#menu_ti}" ; readonly mt_len
sep_len="$(( mt_len + 5 ))" ; readonly sep_len
und_str="$(printf -- "${und_char}%.0s" $(seq 1 "$(( mt_len ))"))"
readonly und_str
sep_str="$(printf -- "${sep_char}%.0s" $(seq 1 "$(( sep_len ))"))"
readonly sep_str

# Define menu string
menu_str="
${sep_str}

${menu_ti}
${und_str}
1) System Percent Utilization (5 minutes)
2) All Processes, All Users
3) Real-Time Process Information
4) PIDs Tied to a Process Name
5) Process Line For PID(s)
6) Kill a Process
7) Renice a Process
q) Quit

Enter menu selection:\
" ; readonly menu_str


#############
# Functions #
#############
#######################################
# Present menu.
# Globals:
#   fb
#   fr
#   menu_str
#######################################
menu() {
    local err_msg force_kill menu_answer nice_value process_ids process_name \
        process_pid spu

    echo "${menu_str}"  # Display menu options

    read -r menu_answer  # Save input

    # Run commands that correspond to menu choice
    case "${menu_answer}" in
        '1')
            spu="($(uptime | awk -F , '{ print $(NF - 1) }') / $(nproc)) * 100"
            {
                echo -e -n '\nSystem percent utilization: '
                echo "${spu}" | bc -l | awk '{ printf("%.2f\n", $1) }'
            } | less

            menu
            ;;
        '2')
            ps -efl | less

            menu
            ;;
        '3')
            top

            menu
            ;;
        '4')
            # Prompt for input, save to variable
            echo ''
            read -p 'Process name string? ' -r process_name

            pgrep -a "${process_name}" | less

            menu
            ;;
        '5')
            echo ''
            read -p 'Process ID(s)? ' -r process_ids
            process_ids="${process_ids//[^0-9 ]/}"  # Sanitize input
            # shellcheck disable=SC2086
            ps ${process_ids} | less

            menu
            ;;
        '6')
            echo ''
            read -p 'Process ID? ' -r process_pid
            process_pid="${process_pid//[^0-9]/}"

            while true; do
                read -p 'Force kill? (y or n) ' -r force_kill

                if [[ "${force_kill}" == 'y' || "${force_kill}" == 'n' ]]; then
                    break
                elif [[ "${force_kill}" == 'q' ]]; then
                    exit 0
                else
                    err_msg='\nInvalid input. Try again or enter q to quit.\n'
                    echo -e "${err_msg}" 1>&2
                fi
            done

            if [[ "${force_kill}" == 'y' ]]; then
                sudo kill -SIGKILL "${process_pid}"
            elif [[ "${force_kill}" == 'n' ]]; then
                sudo kill "${process_pid}"
            fi

            menu
            ;;
        '7')
            echo ''
            read -p 'Process ID? ' -r process_pid
            process_pid="${process_pid//[^0-9]/}"

            read -p 'New nice value? ' -r nice_value
            nice_value="${nice_value//[^0-9]/}"

            sudo renice "${nice_value}" "${process_pid}" | less

            menu
            ;;
        'q')
            :
            ;;
        *)
            echo -e "\n${fb}Please enter a valid menu selection.${fr}" 1>&2

            menu
            ;;
    esac
}


#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    menu  # Call menu function
}


###########
# Program #
###########
main "${@}"  # Start program
