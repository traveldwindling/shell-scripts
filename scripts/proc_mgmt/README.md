# Process Management

Display and manage processes.

## Sample Output

```console

***********************

Process Management
~~~~~~~~~~~~~~~~~~
1) System Percent Utilization (5 minutes)
2) All Processes, All Users
3) Real-Time Process Information
4) PIDs Tied to a Process Name
5) Process Line For PID(s)
6) Kill a Process
7) Renice a Process
q) Quit

Enter menu selection:

```