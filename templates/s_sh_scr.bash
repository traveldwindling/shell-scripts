#!/usr/bin/env bash
###########
# Aliases #
###########
#shopt -s expand_aliases  # Enable alias expansion

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
ft="$(tput sitm)" ; readonly ft
fr="$(tput sgr0)" ; readonly fr

# Create datetime stamp
dt_stamp="$(date +'%Y-%m-%d_%H:%M')" ; readonly dt_stamp
# Create date stamp
d_stamp="$(date +'%Y-%m-%d')" ; readonly d_stamp
# Create UNIX epoch timestamp
tstamp="$(date +'%s')" ; readonly tstamp

# Obtain script root
script_root="${BASH_SOURCE[0]%/*}" ; readonly script_root
# Obtain script name
script_name="${0##/*/}"
script_name="${script_name%.*}" ; readonly script_name

# Set log root
log_root="${script_root}/logs" ; readonly log_root
# Set log directory
log_dir="${log_root}/${script_name}" ; readonly log_dir
# Set log path
log_path="${log_dir}/${script_name}.log" ; readonly log_path
# Set max log size
max_log_size=10485760 ; readonly max_log_size
# Set maximum rolled over log number
max_roll_num=3 ; readonly max_roll_num
# Set excess rolled over log number
exc_roll_num="$(( max_roll_num + 1 ))" ; readonly exc_roll_num
log_dt_fmt="$(date +'%Y-%m-%d %H:%M:%S')" ; readonly log_dt_fmt


#############
# Functions #
#############
#######################################
# Display script usage.
# Globals:
#   log_path
#######################################
display_usage() {
    local help_msg

    help_msg="\
Usage: '${0}'

Purpose: SCRIPT_PURPOSE

A log is saved to '${log_path}'.

Requires:
    PACKAGE
        DESCRIPTION

Variables to set:
    VARIABLE
        DESCRIPTION.

Options:
    -h, --help
        Display script usage.\
"

    echo "${help_msg}"
}


#######################################
# Parse script arguments.
# Arguments:
#   All script arguments.
#######################################
parse_args() {
    local inv_ents parsed_args

    parsed_args="$(getopt \
        -n "${0##*/}" \
        -o h \
        --long help \
        -- "${@}")"

    # Check for invalid entries
    inv_ents="${?}"
    if [[ "${inv_ents}" -ne 0 ]]; then
        echo ''
        display_usage
        exit 1
    fi

    # Set positional parameters using parsed argument string
    eval set -- "${parsed_args}"

    # Evaluate options
    while true; do
        case "${1}" in
            '-h' | '--help')
                display_usage
                exit 0
                ;;
            '--')
                ## End of options
                shift
                break
                ;;
        esac
    done
}


#######################################
# Run initialization checks.
# Globals:
#   ex_var
#######################################
run_init_cks() {
    local ex_var

    :
}


#######################################
# Handle a SIGINT signal.
#######################################
hndl_sigint() {
    # shellcheck disable=SC2317
    echo -e '\n\nSIGINT or Ctrl+c detected. Exiting gracefully.'
    # shellcheck disable=SC2317
    exit 130
}


#######################################
# Initialize logging.
# Globals:
#   exc_roll_num
#   log_dir
#   log_path
#   max_log_size
#   max_roll_num
#   script_name
#######################################
init_logging() {
    local h_log_num i log_name log_names log_nums log_size new_num num \
        roll_over

    # If script log directory does not exist, create it
    if ! [[ -d "${log_dir}" ]]; then
        mkdir -p "${log_dir}"
    fi

    # If script log file does not exist, create it
    if ! [[ -f "${log_path}" ]]; then
        touch "${log_path}"
    fi

    log_size="$(stat -c%s "${log_path}")"  # Get current log size
    # Test whether current log file needs to be rolled over
    roll_over="$([[ "${log_size}" -gt "${max_log_size}" ]] ; echo "${?}")"

    # Declare log file names indexed array
    declare -a log_names
    # Create array of log file names
    # shellcheck disable=SC2061,SC2086
    mapfile -t log_names < \
        <(find "${log_dir}" \
            -mindepth 1 \
            -maxdepth 1 \
            -type f \
            -name ${script_name}.log* \
            -printf '%f\n'
        )

    # If there are rolled over log files
    if [[ "${#log_names[*]}" -gt 1 ]]; then
        # Declare log file numbers indexed array
        declare -a log_nums
        for log_name in "${log_names[@]}"; do
            if [[ "${log_name}" =~ ${script_name}.log.[1-"${max_roll_num}"] ]]
            then
                log_nums+=("${log_name##*.}")
            fi
        done

        # If there are rolled over log files, find highest log file number
        if [[ "${log_nums[*]}" ]]; then
            h_log_num="${log_nums[0]}"

            for num in "${log_nums[@]}"; do
                [[ "${num}" -gt "${h_log_num}" ]] && h_log_num="${num}"
            done
        fi

        # If the current log needs to be rolled over
        if [[ "${roll_over}" -eq 0 ]]; then
            # Roll over rolled over log files
            for (( i="${h_log_num}"; i >= 1; i-- )); do
                new_num="$(( i + 1 ))"
                mv "${log_path}.${i}" "${log_path}.${new_num}"
            done
        fi

        # Remove excess rolled over file
        if [[ -f  "${log_path}.${exc_roll_num}" ]]; then
            rm "${log_path}.${exc_roll_num}"
        fi
    fi

    if [[ "${roll_over}" -eq 0 ]]; then
        mv "${log_path}" "${log_path}.1"  # Roll over current log file
        touch "${log_path}"  # Re-create current log file
    fi
}


#######################################
# Log a message.
# Globals:
#   log_dt_fmt
#   log_path
# Arguments:
#   Log level.
#   Log message.
#######################################
log_msg() {
    local log_level log_message

    log_level="${1}"
    log_message="${2}"
    echo "${log_dt_fmt} - ${log_level} - ${log_message}" >> "${log_path}"
}


# Validation Functions Start #
# Validation Functions End #


# Set Functions Start #
# Set Functions End #


#######################################
# Define starting point for execution of the program.
# Globals:
#   ex_var
# Arguments:
#   All script arguments.
#######################################
main() {
    parse_args "${@}"

    run_init_cks

    trap hndl_sigint SIGINT

    init_logging

    log_msg 'DEBUG' 'Starting program'

    log_msg 'DEBUG' 'Ending program'
}


###########
# Program #
###########
main "${@}"  # Start program
