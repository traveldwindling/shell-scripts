#!/usr/bin/env bash
# Purpose: Template for testing Bash code.
# set -x  # Enable tracing

###########
# Aliases #
###########
# shopt -s expand_aliases  # Enable alias expansion

#############
# Variables #
#############
# Define formatting variables
fb="$(tput bold)" ; readonly fb
ft="$(tput sitm)" ; readonly ft
fr="$(tput sgr0)" ; readonly fr

# Create datetime stamp
dt_stamp="$(date +'%Y-%m-%d_%H:%M')" ; readonly dt_stamp
# Create date stamp
d_stamp="$(date +'%Y-%m-%d')" ; readonly d_stamp
# Create UNIX epoch timestamp
tstamp="$(date +'%s')" ; readonly tstamp

# Obtain script root
script_root="${BASH_SOURCE[0]%/*}" ; readonly script_root
# Obtain script name
script_name="${0##/*/}"
script_name="${script_name%.*}" ; readonly script_name


#############
# Functions #
#############
#######################################
# Define starting point for execution of the program.
# Arguments:
#   All script arguments.
#######################################
main() {
    :
}


###########
# Program #
###########
main "${@}"  # Start program
# set +x  # Disable tracing
