# Templates

Shell script templates.

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="s_sh_scr.bash"><code>s_sh_scr.bash</code></a></td>
      <td>Shell script template.</td>
    </tr>
    <tr>
      <td><a href="s_sh_test.bash"><code>s_sh_test.bash</code></a></td>
      <td>Template for testing Bash code.</td>
    </tr>
  </tbody>
</table>